import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptor/auth.interceptor';
//import { NotificationInterceptor } from './interceptor/notification.interceptor';
import { ConsentpopoverPageModule } from './consentpopover/consentpopover.module';
import { Facebook } from '@ionic-native/facebook/ngx';


@NgModule({
  declarations: [ AppComponent, ],
  entryComponents: [],
  imports: [ BrowserModule, HttpClientModule, IonicModule.forRoot(), AppRoutingModule, ConsentpopoverPageModule ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  //  { provide: HTTP_INTERCEPTORS, useClass: NotificationInterceptor, multi: true },
    Facebook
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}
