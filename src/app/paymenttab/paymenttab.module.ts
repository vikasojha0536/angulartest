import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymenttabPageRoutingModule } from './paymenttab-routing.module';

import { PaymenttabPage } from './paymenttab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymenttabPageRoutingModule
  ],
  declarations: [PaymenttabPage]
})
export class PaymenttabPageModule {}
