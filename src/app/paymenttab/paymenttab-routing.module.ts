import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymenttabPage } from './paymenttab.page';

const routes: Routes = [
  {
    path: '',
    component: PaymenttabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymenttabPageRoutingModule {}
