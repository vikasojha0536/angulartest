import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymenttabPage } from './paymenttab.page';

describe('PaymenttabPage', () => {
  let component: PaymenttabPage;
  let fixture: ComponentFixture<PaymenttabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymenttabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymenttabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
