import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { environment } from 'src/environments/environment.prod';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  ifwindow = environment.api.window
  userData: any;
    slideOpts = {
        initialSlide: 1,
        speed: 400,
      };

    constructor(private facebook: Facebook) {
    }

    ngOnInit(): void {
        console.warn('in the home page after redirect')
    
    }

    loginWithFB() {
      console.log('here')
      this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
        this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
          this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
        });
      });
    }
}
