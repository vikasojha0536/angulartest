import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-transactions-search',
  templateUrl: './transactions-search.component.html',
  styleUrls: ['./transactions-search.component.scss'],
})
export class TransactionsSearchComponent implements OnInit {


  form: FormGroup;

  constructor(public modalController: ModalController, private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  dismiss() {
    this.modalController.dismiss();
  }

  validate() {
    this.modalController.dismiss({
      'dateFrom': this.form.get('dateFrom').value.substr(0,10),
      'dateTo': this.form.get('dateTo').value.substr(0,10),
    });
  }

  private initForm() {
    this.form = this.formBuilder.group({
      dateFrom: [ '' ],
      dateTo: [ '' ]
    });
  }
}
