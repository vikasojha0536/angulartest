import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountsPageRoutingModule } from './accounts-routing.module';

import { AccountsPage } from './accounts.page';
import { TransactionsSearchComponent } from './transactions-search/transactions-search.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    AccountsPageRoutingModule
  ],
  declarations: [ AccountsPage, TransactionsComponent, TransactionsSearchComponent, WelcomeComponent ],
  entryComponents: [ TransactionsSearchComponent ]
})
export class AccountsPageModule {}
