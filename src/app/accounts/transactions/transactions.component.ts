import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';


import {  BGAccount } from '../../models/account.model';


import { BgTransaction,BGBalanceTransactions, Transactions } from '../../models/transaction.model';
import { AisService } from '../../services/ais.service';
import { TransactionsSearchComponent } from '../transactions-search/transactions-search.component';
import { from} from 'rxjs';
import {  map ,reduce} from 'rxjs/operators';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

    @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;


  bgAccount: BGAccount;

  bgTransactions: Transactions;
  bgBalanceTransactions: BGBalanceTransactions;

    currentRange: string = '0-49';

  constructor(private router: Router, private route: ActivatedRoute, private accountsService: AisService, public modalController: ModalController,
    private loadingService: LoadingService) {
  }

    ngOnInit() {

      


      this.bgAccount = this.router.getCurrentNavigation().extras.state.account;


      this.route.data.subscribe(d => {this.bgBalanceTransactions = d.transactions
    this.bgTransactions = this.bgBalanceTransactions.transactions;
    });
    }

  

    getLastUpdate(): Date {
        return this.bgTransactions.booked[0].bookingDate;
    }

    getIcon(transaction: BgTransaction): string {
        return  Number(transaction.transactionAmount.amount) < 0 ? 'expenses-icon' : 'income-icon';
    }


    getBgTotal(): number {
        var num = 1;
        from(this.bgAccount.balances).pipe(
         map(a => a.balanceAmount),
         map(a => +a.amount),
         reduce((total, balance) => total + balance)
       ).subscribe(t => num = t);
       return num;
     }

    doRefresh(event) {
      this.loadingService.disableLoading();
      this.accountsService.findAllBgTransactions(this.bgAccount.resourceId, '2020-07-01', '')
          .subscribe(t => {
                this.bgBalanceTransactions = t;
                this.loadingService.enableLoading();
                event.target.complete();
            });
    }

    loadData(event) {
      this.loadingService.disableLoading();
        const rangFirstPart = +this.currentRange.split('-')[0] + 50;
        const rangSecondPart = +this.currentRange.split('-')[1] + 50;
        this.currentRange = rangFirstPart + '-' + rangSecondPart;
      this.accountsService.findAllBgTransactions(this.bgAccount.resourceId,'2020-07-01', '', this.currentRange)
          .subscribe(t => {
                this.bgTransactions.booked = this.bgTransactions.booked.concat(t.transactions.booked);
                this.bgTransactions.pending = this.bgTransactions.booked.concat(t.transactions.pending);
                this.loadingService.enableLoading();
                event.target.complete();
            });
    }

    async filter(account) {
        const modal = await this.modalController.create({
            component: TransactionsSearchComponent
            
        });
        modal.onDidDismiss().then(d => {
          this.accountsService.findAllBgTransactions(this.bgAccount.resourceId,  d.data.dateFrom, d.data.dateTo)
              .subscribe(t => this.bgBalanceTransactions = t);
        });
        return await modal.present();
    }

    search(event: CustomEvent) {
        const value  = event.detail.value
        if(value == null || value == ""){
            this.accountsService.findAllBgTransactions(this.bgAccount.resourceId, '2020-07-01', '')
            .subscribe(t => {this.bgTransactions.booked = t.transactions.booked
              });
        }
else{
     this.bgTransactions.booked = this.bgTransactions.booked.filter(v => v.entryReference === value)
}
    }
}
