import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionsResolverGuard } from '../guards/transactions-resolver.guard';

import { AccountsPage } from './accounts.page';
import { TransactionsComponent } from './transactions/transactions.component';
import { AccountsResolverGuard } from '../guards/accounts-resolver.guard';
import { DashboardGuard } from '../guards/dashboard.guard';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: '', component: AccountsPage , resolve: { data: AccountsResolverGuard }, canActivate: [DashboardGuard]},
  { path: ':id/transactions', component: TransactionsComponent, resolve: { transactions: TransactionsResolverGuard } },
   {path: 'welcome', component: WelcomeComponent}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AccountsPageRoutingModule {}
