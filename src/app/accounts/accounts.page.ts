


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import {  concatMap, filter, map, reduce, toArray } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { BGAccounts, BgBalance, BGAccount } from '../models/account.model';

import { Consent } from '../models/consent.model';

import { AisService } from '../services/ais.service';
import { ConsentsService } from '../services/consents.service';
import { ToastController, AlertController } from '@ionic/angular';
import { LoadingService } from '../services/loading.service';




@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.page.html',
  styleUrls: [ './accounts.page.scss' ]
})
export class AccountsPage implements OnInit {

  total: number;
  accounts: BGAccounts;
  type: 'ALL' | 'SVGS' | 'CACC' | 'ODFT' = 'ALL';
  consents: Consent[] = JSON.parse(sessionStorage.getItem('consents'));
  allaccount = sessionStorage.getItem('allaccounts')
  constructor(private route: ActivatedRoute, private router: Router, 
    private accountsService: AisService, private consentsService: ConsentsService,
    public alertController: AlertController, private loadingService: LoadingService) {
  }

  ngOnInit() {
  
this.loadingService.enableLoading();

console.warn('in the accounts page')
    this.route.data.subscribe(d => {
      this.accounts = d.data;

     // console.warn('iban is sss'+this.accounts.accounts[0].balances[0].balanceAmount)

     if(this.allaccount == null || this.allaccount == undefined){
      this.getTotal()
     }
    });


  }



  goToTransactions(account: BGAccount) {
    this.router.navigate(['tabs', 'tab', 'accounts', account.resourceId, 'transactions'], {state: {account: account}});
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Adding Consent will <strong>invalidate</strong> the previous consent!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            sessionStorage.removeItem('consentId');
            sessionStorage.removeItem('Authtoken');
            this.router.navigate(['/consents']);
          
           
          }
        }
      ]
    });

    await alert.present();
  }


  async presentAlertConfirmForDelete() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Deleting Consent will <strong>invalidate</strong> the underlying consent!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
           this.accountsService.delete().subscribe(c => console.log('consent deleted'));
           sessionStorage.removeItem('consentId');
           sessionStorage.removeItem('Authtoken');
           this.router.navigate(['/welcome']);
          }
        }
      ]
    });

    await alert.present();
  }

  getCurrentAccountBalance(balances: BgBalance[]){
    if(this.allaccount == null){
      return this.getBgTotal(balances);
    }
    
  }

  getBgTotal(balances: BgBalance[]): number {
     var num = 1;
     from(balances).pipe(
      map(a => a.balanceAmount),
      map(a => +a.amount),
      reduce((total, balance) => total + balance)
    ).subscribe(t => num = t);
    return num;
  }


  getTotal(): void {
    from(this.accounts.accounts).pipe(
      concatMap(a => a.balances),
      map(a => +a.balanceAmount.amount),
      reduce((total, balance) => total + balance)
    ).subscribe(t => this.total = t);
  }

  segmentChanged(event: CustomEvent) {
    const typenew = event.detail.value;
    this.type = typenew
  //  console.log('segnebt changed' +this.type)
   if ('ALL' === typenew) {
     this.findAll().subscribe(d => {
       this.accounts.accounts = d.accounts;
      console.warn(); 
      this.getTotal()});
    }
     else {
      this.findByType(typenew).subscribe(() => 
      {
        if(this.allaccount == null)
        {
      this.getTotal()
        }
      }
      );
    }
  
}

  doRefresh(event) {
    this.loadingService.disableLoading();
    this.findAll().subscribe(() => {
      if(this.allaccount == null)
      {
    this.getTotal()
      }
      this.loadingService.enableLoading();
      event.target.complete();
    });
  }



  private findAll(): Observable<BGAccounts> {
    return this.accountsService.findBgAccounts();
  }

  private findByType(type: 'ALL' | 'SVGS' | 'CACC' | 'LOAN'): Observable<BGAccount[]> {
    return this.findAll().pipe(
      concatMap(a => a.accounts),
      filter(a => a.cashAccountType === type),
      toArray(),
      map(accounts => this.accounts.accounts = accounts)
    );
  }
}
