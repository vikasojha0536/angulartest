import { NgModule, Injectable, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, ToastController } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';


@Injectable()
class MyErrorHandler implements ErrorHandler {
constructor(private toastController: ToastController){

}
   handleError(error) {
this.printError()
  }

  async printError(){
    const toast = await this.toastController.create({
      message : "username or password incorrect",
        duration: 2000,
        position: 'top',

    });
    toast.present();
  }


}



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
  
    ReactiveFormsModule

  ],
  declarations: [LoginPage],
  providers: [{provide: ErrorHandler, useClass: MyErrorHandler}]
})
export class LoginPageModule {}
