import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { of ,throwError as observableThrowError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;
  inputType = 'password';
  constructor(private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.initForm();
  }
  initForm(): FormGroup {
   
      return this.formBuilder.group({
       username: [null, Validators.required],
       password: [null, Validators.required],
       account: false
      });
    
  }


  dismiss(){
    this.router.navigate(['/home']);
    
  }
 

  login(){
    const name = this.form.get('username').value
      if(name === 'admin'){
        this.router.navigate(['/tabs']);

        sessionStorage.setItem('login', "true");
      }
      else{
      
        throw  observableThrowError("Username or email not found.");
      }
  }
}
