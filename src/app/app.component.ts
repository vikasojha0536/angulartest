import { Component, ViewChild } from '@angular/core';

import { Platform, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  @ViewChild(IonRouterOutlet, {static : false}) routeroutlet: IonRouterOutlet
  ais = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private loading: LoadingService
  ) {
    this.initializeApp();
  }

  aisSelected(){
    this.ais = !this.ais;
  }

  initializeApp() {
    this.platform.backButton.subscribeWithPriority(0, async() => {
      if(this.loading.isLoadingShown()){
console.log('isloading is shown')
      }
      else{
          if(this.routeroutlet && this.routeroutlet.canGoBack()){
            console.log('isloading is not shown')
              this.routeroutlet.pop();
          }
      }
    })
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
