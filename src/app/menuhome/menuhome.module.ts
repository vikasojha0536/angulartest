import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuhomePageRoutingModule } from './menuhome-routing.module';

import { MenuhomePage } from './menuhome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuhomePageRoutingModule
  ],
  declarations: [MenuhomePage]
})
export class MenuhomePageModule {}
