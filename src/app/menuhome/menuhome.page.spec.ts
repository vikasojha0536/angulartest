import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuhomePage } from './menuhome.page';

describe('MenuhomePage', () => {
  let component: MenuhomePage;
  let fixture: ComponentFixture<MenuhomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuhomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuhomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
