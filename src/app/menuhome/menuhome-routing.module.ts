import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuhomePage } from './menuhome.page';

const routes: Routes = [
  {
    path: '',
    component: MenuhomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuhomePageRoutingModule {}
