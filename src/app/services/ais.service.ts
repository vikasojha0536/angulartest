import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError, EMPTY } from 'rxjs';
import { catchError} from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { BGAccounts, scaPatchResponse } from '../models/account.model';
import { PostConsent, ScaMethodPatch } from '../models/consent.model';

import { BookingStatus,BGBalanceTransactions } from '../models/transaction.model';
import { Token, Metadata } from '../models/authorisation.model';
import { ConsentsService } from './consents.service';
import { SessionService } from './session.service';
import {KJUR, KEYUTIL} from 'jsrsasign';

import * as CryptoJs from 'crypto-js';
import * as shajs from 'sha.js'
const DATE_FROM = 'dateFrom';
const DATE_TO = 'toDate';
const BOOKING_STATUS = 'bookingStatus';
const RANGE = 'range';
const QUERY_STRING = 'queryString';

@Injectable({
  providedIn: 'root'
})

export class AisService {

  
  private host = environment.api.host
  private url = this.host + environment.api.basepath.ais;
  private urllogin = this.host + environment.api.basepath.bgauth;
  private resourceName = environment.api.resourceName;
  
  constructor(private http: HttpClient, private sessionService: SessionService) {}



  findBgAccounts(): Observable<BGAccounts> {
    const allaccounts = sessionStorage.getItem('allaccounts')
    var httpParams = null;
    if(allaccounts == null || allaccounts == undefined){
       httpParams = new HttpParams()
        .set('withBalance', 'true')
    }
     const options = {
      params: httpParams
    
    };
  

    return this.http.get<BGAccounts>(this.url + this.resourceName.accounts, options).pipe(
      catchError(e => { return observableThrowError(e); })
    );
  }

  token(body: string, headers: HttpHeaders): Observable<Token> {
  
    return this.http.post<Token>(this.urllogin  + this.resourceName.token, body, { headers: headers })
               .pipe(
                 catchError(e => { return observableThrowError(e); })
               );
  }

  delete(): Observable<any>{
    return this.http.delete(this.url  + this.resourceName.consent+ '/'+this.sessionService.getLastAddedConsentId())
    .pipe(
      catchError(e => { return observableThrowError(e); })
    );
  }

  refreshToken(token: string, headers: HttpHeaders): Observable<Token> {

    if(token == null || token == undefined){
return EMPTY
    }

     const body = new HttpParams()
    .set('refresh_token', token)
    .set('grant_type', 'refresh_token')
    return this.http.post<Token>(this.urllogin  + this.resourceName.token, body.toString(), { headers: headers })
               .pipe(
                 catchError(e => { return observableThrowError(e); })
               );
  }


  findAllBgTransactions(resourceId: string,  dateFrom: string, dateTo: string = '', range: string = '0-49', query?: string): Observable<BGBalanceTransactions> {
    let params = new HttpParams()
                                 .set(DATE_FROM, dateFrom)
                                 .set(BOOKING_STATUS, BookingStatus.BOTH)         
                                 .set(RANGE, range);
    if (dateTo !== '') {
      params = params.set(DATE_TO, dateTo);
    }
    if (query) {
      params = params.set(QUERY_STRING, query);
    }
    return this.http.get<BGBalanceTransactions>(this.url + this.resourceName.accounts +'/' + resourceId + this.resourceName.transactions, { params: params }).pipe(

      catchError(e => { return observableThrowError(e); })
    );
  }

  
  put(scaPatch: ScaMethodPatch, href: string): Observable<scaPatchResponse> {
    return this.http.put<scaPatchResponse>(this.host+'berlingroup'+href, scaPatch);
  }


  getMetaData(metaDataUrl: string): Observable<Metadata> {
    console.log('in the metadata' +metaDataUrl)
    return this.http.get<Metadata>(metaDataUrl);
  }


  createPost(consent: PostConsent): Observable<any> {

    //var crypto = require('crypto');

    // var sha_hash = CryptoJs.createHash('SHA256');
    // sha_hash.update(consent);
    // var stoken = sha_hash.digest('hex');

    // var signature = crypto.createSign('RSA-SHA256')
    // signature.update(stoken);
    // var sig = signature.sign("MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDDF+fOfY8APqV5oIySFc+FQR5SWXBO/0oKIrLtCzTOlzNAXGPRgUaJifCipaFjLVq/4J91Q01q1lkJ0BBAjUKM7ZeOOatjBOOFy7OPhxIPxrxqIXET1y7uMsxr98NPnfadIzEiXijibV5nd4+mqRAdV87pjV0H6hzPqhNY+Sn7dznpMtrsYGjq/t5Ur48VFnYfP0P5N1tIut4gr1WZf/mlpXAArNO0hJ3DtEP2FMj0LiyAax2hBVQSnRvKCIJOJ1HBtD8N8I8K56KXvHnnXr/8b14dhvazkN/LWaRW7TSC3vjcWP8zPb1iPouJ3g09MZR3Kw81BfA0coqQzBzvuzIDAgMBAAECggEBAIk9Cc4SOUQ8zX7v17L0MNVWZ5QaYozPvS/AFQn3NiCV8HG4DGfWft5F8McZqJvxzcLoqoN84rrjKiNi/+vI8yXcBR8BpnSzfbIjB59Rrvz2Gpi2CauhVdwuU+snkXcyccc8eFXq0uvlb+FtE9pzV2is2lslHGpy3+2dgJq4PDYt/vsT6cu24carfRR37N0UDTiqmRyz8fd3EqA6DuaM4dE7+R4G9ijI/C3+PRIYkWYclyA8tOnUggvUhvH+EyC85MWwYW0drEC/xTdwu20VsjyJuV40r0EHC484JHcAYzE0tI+H1EnYaN/2gVzswUFqhvjn+knl5kFdaYGfyziih6ECgYEA9XPhjW4HGtzIqjA6gHNqW6ds4f7mn4rJiM2plA2W6TrYHEYWxO0cvpoRGlVfuExwINKS2Ukg1lvxC2fmOoQOuVx/LjtyYTA2zZGKxqHeWpch1h06jd6ZJZP7fH7chgf1MEB/b39sMIvFJN4wXodwCAfq9oRXpM8Q5/++nsYnm3sCgYEAy3oLL9fV7+coHJ7cIkWqq0ioDHHaODVB+7F2ZWBECna98Y2yMA2nnRxoc46pj+9lqKIEnf9fEQqVnWbqM3M/n7MhXnGLT0fIcXniNhDMWjuq2phvyNUFhH76zgk8qNRcJw0gfsG68iG94hqO5WqPUJmAsTqoinV5vauOF+iqGRkCgYAqos5zOsGRMPjKV0ZSObon4ZVQTG7TF9CaKt9iEFo+eTJA9wvNeUKc+TYvVNUjtmNj0DAh/knt8lqUmJ+tWnMUT7Tn2vurViyu2LE6f7OYGNiP0NPzAM+pb2Mn5QF5Zrh4gTuhqoiohh7goJfWU8BqJLXzzFlND4roQcxi2BjZMQKBgQCt+OFLvLz8a6XOIr2QyAQOgwa77TRpTo/mBWt3bgF++Nuahk5N677eBAYCl32+xS8kKbLM7CV6SpR1iiLAbs99Wgkz2N0FohhtfYq+M0ql89I3KJKbIhefm3oRY7BroXZRJVYdSsLud48EnTSETsHOZhC0d7TW/W5jjyTkqFeXMQKBgDk+K07lSXO4DnKZOuhbSDLQq/rPTbkUvsPDKoEPt5Bcw0BQrQyzv3GzmfZwSEaTKjCKHZ0Spy39yxZfhuMXZ/p4E/RNqWfxitwTpSXS7Fm2NuEig5eu4yXiGThHsF44JCzk3u0SO3/wQaQFT9vYXpDlXOUmKo+E+5fwn3wydX+q", 'base64');




   //const temp = shajs('sha256').update(JSON.stringify(consent)).digest();
    const digest =  "SHA-256=" + btoa(JSON.stringify(consent))
    const unencryted = "digest: "+ digest+"\nx-request-id: 4bb64c99-1862-1c7a-b567-c8243ebe3cb7";
    const val =  CryptoJs.AES.encrypt(unencryted, "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDDF+fOfY8APqV5oIySFc+FQR5SWXBO/0oKIrLtCzTOlzNAXGPRgUaJifCipaFjLVq/4J91Q01q1lkJ0BBAjUKM7ZeOOatjBOOFy7OPhxIPxrxqIXET1y7uMsxr98NPnfadIzEiXijibV5nd4+mqRAdV87pjV0H6hzPqhNY+Sn7dznpMtrsYGjq/t5Ur48VFnYfP0P5N1tIut4gr1WZf/mlpXAArNO0hJ3DtEP2FMj0LiyAax2hBVQSnRvKCIJOJ1HBtD8N8I8K56KXvHnnXr/8b14dhvazkN/LWaRW7TSC3vjcWP8zPb1iPouJ3g09MZR3Kw81BfA0coqQzBzvuzIDAgMBAAECggEBAIk9Cc4SOUQ8zX7v17L0MNVWZ5QaYozPvS/AFQn3NiCV8HG4DGfWft5F8McZqJvxzcLoqoN84rrjKiNi/+vI8yXcBR8BpnSzfbIjB59Rrvz2Gpi2CauhVdwuU+snkXcyccc8eFXq0uvlb+FtE9pzV2is2lslHGpy3+2dgJq4PDYt/vsT6cu24carfRR37N0UDTiqmRyz8fd3EqA6DuaM4dE7+R4G9ijI/C3+PRIYkWYclyA8tOnUggvUhvH+EyC85MWwYW0drEC/xTdwu20VsjyJuV40r0EHC484JHcAYzE0tI+H1EnYaN/2gVzswUFqhvjn+knl5kFdaYGfyziih6ECgYEA9XPhjW4HGtzIqjA6gHNqW6ds4f7mn4rJiM2plA2W6TrYHEYWxO0cvpoRGlVfuExwINKS2Ukg1lvxC2fmOoQOuVx/LjtyYTA2zZGKxqHeWpch1h06jd6ZJZP7fH7chgf1MEB/b39sMIvFJN4wXodwCAfq9oRXpM8Q5/++nsYnm3sCgYEAy3oLL9fV7+coHJ7cIkWqq0ioDHHaODVB+7F2ZWBECna98Y2yMA2nnRxoc46pj+9lqKIEnf9fEQqVnWbqM3M/n7MhXnGLT0fIcXniNhDMWjuq2phvyNUFhH76zgk8qNRcJw0gfsG68iG94hqO5WqPUJmAsTqoinV5vauOF+iqGRkCgYAqos5zOsGRMPjKV0ZSObon4ZVQTG7TF9CaKt9iEFo+eTJA9wvNeUKc+TYvVNUjtmNj0DAh/knt8lqUmJ+tWnMUT7Tn2vurViyu2LE6f7OYGNiP0NPzAM+pb2Mn5QF5Zrh4gTuhqoiohh7goJfWU8BqJLXzzFlND4roQcxi2BjZMQKBgQCt+OFLvLz8a6XOIr2QyAQOgwa77TRpTo/mBWt3bgF++Nuahk5N677eBAYCl32+xS8kKbLM7CV6SpR1iiLAbs99Wgkz2N0FohhtfYq+M0ql89I3KJKbIhefm3oRY7BroXZRJVYdSsLud48EnTSETsHOZhC0d7TW/W5jjyTkqFeXMQKBgDk+K07lSXO4DnKZOuhbSDLQq/rPTbkUvsPDKoEPt5Bcw0BQrQyzv3GzmfZwSEaTKjCKHZ0Spy39yxZfhuMXZ/p4E/RNqWfxitwTpSXS7Fm2NuEig5eu4yXiGThHsF44JCzk3u0SO3/wQaQFT9vYXpDlXOUmKo+E+5fwn3wydX+q")
 //   const sign = btoa(val.toString())
 
 //var priv = new KEYUTIL


      // initialize
var sig = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});
// initialize for signature generation
sig.init("-----BEGIN RSA PRIVATE KEY-----MIIEpAIBAAKCAQEAwxfnzn2PAD6leaCMkhXPhUEeUllwTv9KCiKy7Qs0zpczQFxj0YFGiYnwoqWhYy1av+CfdUNNatZZCdAQQI1CjO2XjjmrYwTjhcuzj4cSD8a8aiFxE9cu7jLMa/fDT532nSMxIl4o4m1eZ3ePpqkQHVfO6Y1dB+ocz6oTWPkp+3c56TLa7GBo6v7eVK+PFRZ2Hz9D+TdbSLreIK9VmX/5paVwAKzTtISdw7RD9hTI9C4sgGsdoQVUEp0bygiCTidRwbQ/DfCPCueil7x5516//G9eHYb2s5Dfy1mkVu00gt743Fj/Mz29Yj6Lid4NPTGUdysPNQXwNHKKkMwc77syAwIDAQABAoIBAQCJPQnOEjlEPM1+79ey9DDVVmeUGmKMz70vwBUJ9zYglfBxuAxn1n7eRfDHGaib8c3C6KqDfOK64yojYv/ryPMl3AUfAaZ0s32yIwefUa789hqYtgmroVXcLlPrJ5F3MnHHPHhV6tLr5W/hbRPac1dorNpbJRxqct/tnYCauDw2Lf77E+nLtuHGq30Ud+zdFA04qpkcs/H3dxKgOg7mjOHRO/keBvYoyPwt/j0SGJFmHJcgPLTp1IIL1Ibx/hMgvOTFsGFtHaxAv8U3cLttFbI8ibleNK9BBwuPOCR3AGMxNLSPh9RJ2Gjf9oFc7MFBaob45/pJ5eZBXWmBn8s4ooehAoGBAPVz4Y1uBxrcyKowOoBzalunbOH+5p+KyYjNqZQNluk62BxGFsTtHL6aERpVX7hMcCDSktlJINZb8Qtn5jqEDrlcfy47cmEwNs2Risah3lqXIdYdOo3emSWT+3x+3IYH9TBAf29/bDCLxSTeMF6HcAgH6vaEV6TPEOf/vp7GJ5t7AoGBAMt6Cy/X1e/nKBye3CJFqqtIqAxx2jg1QfuxdmVgRAp2vfGNsjANp50caHOOqY/vZaiiBJ3/XxEKlZ1m6jNzP5+zIV5xi09HyHF54jYQzFo7qtqYb8jVBYR++s4JPKjUXCcNIH7BuvIhveIajuVqj1CZgLE6qIp1eb2rjhfoqhkZAoGAKqLOczrBkTD4yldGUjm6J+GVUExu0xfQmirfYhBaPnkyQPcLzXlCnPk2L1TVI7ZjY9AwIf5J7fJalJifrVpzFE+059r7q1YsrtixOn+zmBjYj9DT8wDPqW9jJ+UBeWa4eIE7oaqIqIYe4KCX1lPAaiS188xZTQ+K6EHMYtgY2TECgYEArfjhS7y8/GulziK9kMgEDoMGu+00aU6P5gVrd24BfvjbmoZOTeu+3gQGApd9vsUvJCmyzOwlekqUdYoiwG7PfVoJM9jdBaIYbX2KvjNKpfPSNyiSmyIXn5t6EWOwa6F2USVWHUrC7nePBJ00hE7BzmYQtHe01v1uY48k5KhXlzECgYA5PitO5UlzuA5ymTroW0gy0Kv6z025FL7DwyqBD7eQXMNAUK0Ms79xs5n2cEhGkyowih2dEqct/csWX4bjF2f6eBP0Taln8YrcE6Ul0uxZtjbhIoOXruMl4hk4R7BeOCQs5N7tEjt/8EGkBU/b2F6Q5VzlJiqPhPuX8J98MnV/qg==-----END RSA PRIVATE KEY-----");   // rsaPrivateKey of RSAKey object
// update data
sig.updateString(unencryted);
// calculate signature
var sign = btoa(sig.sign())
const cert = '-----BEGIN CERTIFICATE-----'+
'MIIEgTCCA2mgAwIBAgIIGUucgMjXl/gwDQYJKoZIhvcNAQELBQAwaTELMAkGA1UE'+
'BhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMD'+
'U0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNv'+
'bTAeFw0xOTAyMTQxNDQ4MDBaFw0yODEyMTkxODAwMDBaMIGSMQswCQYDVQQGEwJG'+
'UjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMx'+
'DDAKBgNVBAsTA0R4UDEMMAoGA1UEAxMDU0JTMR4wHAYJKoZIhvcNAQkBFg9yb290'+
'Q0FAcm9vdC5jb20xGTAXBgNVBGETEFBTREJFLUFMTC0xMjM0NTYwggEiMA0GCSqG'+
'SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDDF+fOfY8APqV5oIySFc+FQR5SWXBO/0oK'+
'IrLtCzTOlzNAXGPRgUaJifCipaFjLVq/4J91Q01q1lkJ0BBAjUKM7ZeOOatjBOOF'+
'y7OPhxIPxrxqIXET1y7uMsxr98NPnfadIzEiXijibV5nd4+mqRAdV87pjV0H6hzP'+
'qhNY+Sn7dznpMtrsYGjq/t5Ur48VFnYfP0P5N1tIut4gr1WZf/mlpXAArNO0hJ3D'+
'tEP2FMj0LiyAax2hBVQSnRvKCIJOJ1HBtD8N8I8K56KXvHnnXr/8b14dhvazkN/L'+
'WaRW7TSC3vjcWP8zPb1iPouJ3g09MZR3Kw81BfA0coqQzBzvuzIDAgMBAAGjggEB'+
'MIH+MA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFAbR/SMTq2bQDWHcRqDjqdPM'+
'qbI3MAsGA1UdDwQEAwIBBjAUBgNVHSAEDTALMAkGBwQAi+xAAQEwdgYIKwYBBQUH'+
'AQMEajBoMGYGBgQAgZgnAjBcMEwwEQYHBACBmCcBAQwGUFNQX0FTMBEGBwQAgZgn'+
'AQIMBlBTUF9QSTARBgcEAIGYJwEDDAZQU1BfQUkwEQYHBACBmCcBBAwGUFNQX0lD'+
'DANOQkIMB0JlbGdpdW0wEQYJYIZIAYb4QgEBBAQDAgAHMB4GCWCGSAGG+EIBDQQR'+
'Fg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAHqWshRu3To+O5Zl'+
'DWdyDkc+gHIgD2xBuS0liwSF0yrh7serlzSJsvjWtIYtj6AXmsK8f7SDYkljAItW'+
'6bTZqT8m9vBMGhFN0PyT1BzeDj3uzMoO9/BFOMdJtKeZjbPK1QMWZR1ni1GpP3BM'+
'a1/jvPZUy6y+o6fhlm/JgnS9i2l5I3ZfwrES9l/XTGtH0HwX0v60IW6tHNMF2Ky+'+
'twEmtF8+0dYo12JnoELiHfxHzG9x9/03oj4PjzcruiLXVBUg0+Rnp1/oo60CnpRi'+
'1blppKEZ5HvJtZVphcs22Ny9pkcSVnxzcuf02oSlstUYYWA2LG7o6dKL2jGlDcs3'+
'QxscK8I='+
'-----END CERTIFICATE-----'

// initialize
var sig = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});
// initialize for signature validation
sig.init(cert); // signer's certificate
// update data
sig.updateString(unencryted)
// verify signature
var isValid = sig.verify(atob(sign))
console.log('is valid'+isValid)

    const options = {
      headers: new HttpHeaders()
      .set('signature', 'keyId=\"SN=194b9c80c8d797f8,CA=EMAILADDRESS=rootCA@root.com, OU=DxP, O=SBS, L=Paris, ST=Paris, C=FR\",algorithm=\"rsa-sha256\",headers=\"digest x-request-id\",signature="\"' + sign + '\"')
      .set('digest', digest)
      .set('tpp-signature-certificate', cert)

    }

    return this.http.post(this.url + this.resourceName.consent, consent);
  }

}
