import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';


const PSU_ID = 'psuId';

@Injectable({
  providedIn: 'root'
})
export class ConsentsService {

  private url = environment.api.host + environment.api.resourceName.consent;

  constructor(private http: HttpClient) {
  }



  delete(id: string, psuId: string): Observable<any> {
    const options = {
      params: new HttpParams().set(PSU_ID, psuId)
    };
    return this.http.delete(this.url + '/' + id, options);
  }
}
