import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';





import { ToastController } from '@ionic/angular';
import { NotificationType, NotificationAlert } from '../models/notification-alert.model';
import { LoadingService } from './loading.service';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  defaultErrorType = NotificationType.ERROR;
  defaultMessage: string;
  defaultTitle: string;

  constructor(private toastController: ToastController, private loadingService: LoadingService) {}

  public buildAndShowNotification(response: HttpErrorResponse) {
    let notificationAlerts;
    if (response.error && response.error.tppMessages) {
      notificationAlerts = this.mapFeedbacksToNotificationAlerts(response);
    } else {
      notificationAlerts = this.mapGenericMessagesToNotificationAlerts(response);
    }
    this.showNotificationAlerts(notificationAlerts);
  }

   public showNotificationAlerts(alerts: NotificationAlert[]): void {
    alerts.forEach(async alert => {
      const message = alert.message;
      const title = alert.label;
           const toast = await this.toastController.create({
            message : message,
              duration: 4000,
              position: 'top'

          });
          toast.present();
      }
    );
  }

  private mapFeedbacksToNotificationAlerts(response: HttpErrorResponse): NotificationAlert[] {
    let responseMessage;
    this.loadTextForNotification();
    const notificationArray: NotificationAlert[] = [];
    response.error.tppMessages.forEach(tppMessage => {
      if (response.status >= 400 && response.status < 500) {
        responseMessage = tppMessage.text + '. ' + tppMessage.path + '.' + this.defaultMessage;
      } else {
        responseMessage = this.defaultTitle + this.defaultMessage;
      }
      notificationArray.push(
        {
          referenceId: response.error.ticketId ? response.error.ticketId : '',
          type: response.error.returnCode ? response.error.returnCode : this.defaultErrorType,
          code: tppMessage.code,
          label: tppMessage.text,
          message: responseMessage
        }
      );
    });
    return notificationArray;
  }

  private mapGenericMessagesToNotificationAlerts(response: HttpErrorResponse): NotificationAlert[] {
    let resMessage;
    const notificationArray: NotificationAlert[] = [];

      resMessage = response.message;

    notificationArray.push(
      <NotificationAlert>{
        type: this.defaultErrorType,
        code: response.status,
        message: resMessage
      });
    return notificationArray;
  }

  private loadTextForNotification() {

        this.defaultMessage = ' Please contact customer support with ticket id';
        this.defaultTitle = 'We encounter some issue.';

  }
}
