import { Role } from 'src/app/models/role.model';


export interface OpenidToken {
  id_token: string;
  profile: {
    jti: string;
    sub: string;
    typ: string;
    azp: string;
    auth_time: number;
    session_state: string;
    acr: string;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email: string;
    roles: Role[];
  };
}
