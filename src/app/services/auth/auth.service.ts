import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User, UserManager, UserManagerSettings } from 'oidc-client';
import { Observable } from 'rxjs';


import { OpenidToken } from './openid-token.model';
import { environment } from 'src/environments/environment';
import { JwtToken } from 'src/app/models/jwt-token.model';

const AUTHORIZATION = 'Authorization';
const EXTERNAL_TOKEN = 'external_token';
const EXTERNAL_TOKEN_TYPE = 'external_token_type';
const GRANT_TYPE = 'grant_type';
const SCOPE = 'scope';
const TOKEN_TYPE = 'token_type';

const FAILURE_KEY = 'failureTentatives';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private readonly settings: UserManagerSettings = {
    authority: `${environment.settings.openid.authority}${environment.settings.openid.realm}`,
    client_id: environment.settings.openid.client_id,
    redirect_uri: `${environment.settings.openid.root}auth-callback`,
    post_logout_redirect_uri: `${environment.settings.openid.root}`,
    response_type: environment.settings.openid.response_type,
    scope: environment.settings.openid.scope,
    checkSessionInterval: 60000
  };

  private url: string = environment.settings.api.host + environment.settings.uris.auth;
  private userManager = new UserManager(this.settings);
  private user: User = null;

  constructor(private http: HttpClient, private router: Router) {
    this.userManager.getUser().then(user => {
      this.user = user;
    });
    if (this.getFailureTentatives() === undefined) {
      this.resetFailureTentatives();
    }
  }

  exchangeJwtToken(): Observable<JwtToken> {
    const apiKey = environment.settings.api.key;
    const body = new HttpParams()
      .set(EXTERNAL_TOKEN_TYPE, 'backOfficeToken')
      .set(GRANT_TYPE, 'client_credentials')
      .set(SCOPE, 'bank_user')
      .set(TOKEN_TYPE, 'BANK_USER')
      .set(EXTERNAL_TOKEN, this.getOpenidToken().id_token);
    let params = new HttpParams();
    if (apiKey) {
      params = params.set('KeyId', apiKey);
    }
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      .set(AUTHORIZATION, 'Basic ' + this.getAuthBasic());
    return this.http.post<JwtToken>(this.url, body.toString(), { headers: headers, params: params });
  }

  isLoggedIn(): boolean {
    return this.user != null && !this.user.expired;
  }

  getUser(): any {
    return this.user.profile;
  }

  login(): Promise<void> {
    if (this.getFailureTentatives() <= environment.settings.security.failure_tentatives) {
      return this.userManager.signinRedirect();
    } else {
      this.router.navigate([ 'errors/500' ]);
      this.resetFailureTentatives();
    }
  }

  logout(): Promise<void> {
    sessionStorage.clear();
    return this.userManager.signoutRedirect();
  }

  completeAuthentication(): Promise<void> {
    return this.userManager.signinRedirectCallback().then(user => {
      this.user = user;
      this.exchangeJwtToken().subscribe(t => {
          t.expires_at = new Date().getTime() + t.expires_in;
          sessionStorage.setItem(`jwt.user:${this.settings.authority}:${this.settings.client_id}`, JSON.stringify(t));
          this.router.navigate(['home']);
        },
        e => console.error(e)
      );
    });
  }

  getJwtToken(): JwtToken {
    return JSON.parse(sessionStorage.getItem(`jwt.user:${this.settings.authority}:${this.settings.client_id}`));
  }

  getOpenidToken(): OpenidToken {
    return JSON.parse(sessionStorage.getItem(`oidc.user:${ this.settings.authority }:${ this.settings.client_id }`));
  }

  isJwtValid(): boolean {
    const jwt = this.getJwtToken();
    return jwt != null && new Date().getTime() < jwt.expires_at;
  }

  storeJwtToken(): void {
    this.exchangeJwtToken().subscribe(t => {
        t.expires_at = new Date().getTime() + t.expires_in;
        sessionStorage.setItem(`jwt.user:${this.settings.authority}:${this.settings.client_id}`, JSON.stringify(t));
      },
      e => console.error(e)
    );
  }

  getUserRoles(): string[] {
    return this.user.profile.roles;
  }

  addAuthHeaders(req: HttpRequest<any>) {
    const re = environment.settings.uris.auth;
    if (req.url.search(re) === -1) {
      const token: JwtToken = this.getJwtToken();
      if (token) {
        req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token.access_token) });
      }
    }
    const envPort = environment.settings.api.port;
    if (envPort) {
      req = req.clone({ headers: req.headers.set('X-SBS-ENV-PORT', envPort) });
    }
    const apiKey = environment.settings.api.key;
    if (apiKey) {
      req = req.clone({ params: req.params.set('KeyId', apiKey) });
      // remove when api key can be used as query parameter in Bsummer
      req = req.clone({ headers: req.headers.set('apiKey', apiKey) });
    }
    return req;
  }

  private getAuthBasic() {
    return this.user.profile.data;
  }

  getFailureTentatives(): number {
    if (sessionStorage.getItem(FAILURE_KEY)) {
      return Number.parseInt(sessionStorage.getItem(FAILURE_KEY), 10);
    }
    return undefined;
  }

  incrementFailureTentatives(): void {
    sessionStorage.setItem(FAILURE_KEY, (this.getFailureTentatives() + 1).toString());
  }

  resetFailureTentatives(): void {
    sessionStorage.setItem(FAILURE_KEY, '0');
  }
}
