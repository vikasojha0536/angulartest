import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loadingPresent = false;
  loadingEnabled= true;
  showing = false;
  constructor(private loadingController: LoadingController) { }


  showLoader() {

    this.loadingController.getTop().then(hasLoading => {
      if (!hasLoading) {
        this.loadingController.create({
          spinner: 'bubbles',
          message: 'Please wait ...',
          translucent: true

        }).then(loading => {
          this.showing = true;
          loading.present()});
      }

    });
  }

  hideLoader() {

    this.loadingController.getTop().then(hasLoading => {
      if (hasLoading) {
        this.showing =false;
        this.loadingController.dismiss();
      }
    });
  }

  disableLoading() {
this.loadingEnabled = false;
   
  }

  isLoadingEnabled(){
    return this.loadingEnabled == true;
  }

  isLoadingShown(){
    return this.showing == true;
  }

  
  enableLoading() {
    this.loadingEnabled = true;
       
      }

}

