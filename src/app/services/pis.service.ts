import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { BgPayment, PaymentDeleteResponse } from '../models/payment.model';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';
import { Token } from '../models/authorisation.model';
import { CancelAuthResponse } from '../models/consent.model';



@Injectable({
  providedIn: 'root'
})
export class PisService {

  private url = environment.api.host + environment.api.basepath.pis;
  private urllogin = environment.api.host + environment.api.basepath.bgauth;
  private resourceName = environment.api.resourceName;

  constructor(private http: HttpClient) {}

  bgCreate(payment: BgPayment, paymentType: string): Observable<any> {
    //TODO remove when connected to bsummer
    var service = ''
    if(paymentType == 'STANDING ORDER PAYMENT'){
      service =  environment.api.resourceName.periodicPayment
    }
    else{
      service =  environment.api.resourceName.sepaPayment
    }
    const options = {
      headers: new HttpHeaders().set('content-type', 'application/json;charset=UTF-8')
    };
    return this.http.post(this.url + service, payment, options);
  }

  getStatus(paymentId: string, paymentType: string): Observable<any> {
    var service = ''
    if(paymentType == 'STANDING ORDER PAYMENT'){
      service =  environment.api.resourceName.periodicPayment
    }
    else{
      service =  environment.api.resourceName.sepaPayment
    }
    return this.http.get<any>(this.url + service + '/'+paymentId +'/status').pipe(

      catchError(e => { return observableThrowError(e); })
    );
  }

  
  getPeriodicStatus(paymentId: string): Observable<any> {
    
    return this.http.get<any>(this.url + environment.api.resourceName.periodicPayment + '/'+paymentId +'/status').pipe(

      catchError(e => { return observableThrowError(e); })
    );
  }

  getPayment(paymentId: string, paymentType: string): Observable<BgPayment> {
    var service = ''
    if(paymentType == 'STANDING ORDER PAYMENT'){
      service =  environment.api.resourceName.periodicPayment
    }
    else{
      service =  environment.api.resourceName.sepaPayment
    }
    return this.http.get<BgPayment>(this.url + service + '/'+paymentId).pipe(

      catchError(e => { return observableThrowError(e); })
    );
  }

  getPeriodicPayment(paymentId: string): Observable<BgPayment> {

    
    return this.http.get<BgPayment>(this.url + environment.api.resourceName.periodicPayment + '/'+paymentId).pipe(

      catchError(e => { return observableThrowError(e); })
    );
  }

  token(body: string, headers: HttpHeaders): Observable<Token> {
  
    return this.http.post<Token>(this.urllogin  + this.resourceName.token, body, { headers: headers })
               .pipe(
                 catchError(e => { return observableThrowError(e); })
               );
  }

  delete(paymentId: string, paymentType: string): Observable<PaymentDeleteResponse>{
    var service = ''
    if(paymentType == 'STANDING ORDER PAYMENT'){
      service =  environment.api.resourceName.periodicPayment
    }
    else{
      service =  environment.api.resourceName.sepaPayment
    }
    return this.http.delete<PaymentDeleteResponse>(this.url + service + '/'+paymentId)
    .pipe(
      catchError(e => { return observableThrowError(e); })
    );
  }

  deletePeriodic(paymentId: string): Observable<PaymentDeleteResponse>{
  
    return this.http.delete<PaymentDeleteResponse>(this.url + environment.api.resourceName.periodicPayment + '/'+paymentId)
    .pipe(
      catchError(e => { return observableThrowError(e); })
    );
  }

  cancelAuth(href: string) : Observable<CancelAuthResponse>{
    const options = {
      headers: new HttpHeaders().set('content-type', 'application/json;charset=UTF-8')
    };
    return this.http.post<CancelAuthResponse>(environment.api.host+'berlingroup'+href, null, options)
    .pipe(
      catchError(e => { return observableThrowError(e); })
    ); 
  }

}
