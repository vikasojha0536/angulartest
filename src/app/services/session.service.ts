import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }


  storeConsent(consentId: string) {
    sessionStorage.setItem('consentId', consentId);
  }

  getLastAddedConsentId(): string {
    return sessionStorage.getItem('consentId')
  }
  storePaymentId(paymentId: string) {
    sessionStorage.setItem('paymentId', paymentId);
  }
  storePaymentType(paymentType: string) {
    sessionStorage.setItem('paymentType', paymentType);
  }

  storeIfDeletePayment(){
    sessionStorage.setItem('deletepayment', 'true');
  }

  storePaymentToken(access_token: string) {
    sessionStorage.setItem('PaymentToken', access_token)
  }
  storeAllAccounts(){
  sessionStorage.setItem('allaccounts', 'all-accounts');
  }
  storeAccountToken(access_token: string){
  sessionStorage.setItem('Authtoken', access_token)
  }
}
