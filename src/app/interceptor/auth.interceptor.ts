import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { environment } from '../../environments/environment';
import { LoadingService } from '../services/loading.service';
import { catchError, finalize } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';

const AUTHORIZATION = 'Authorization';
const X_SBS_ENV_PORT = 'X-Sbs-Env-Port';
const X_CLIENT_IP_ADDRESS = 'X-Client-Ip-Address';
const API_KEY = 'apiKey';
const X_REQUEST_ID = 'x-request-id';
const X_CLIENT_TRACE_ID = 'x-client-trace-id';
const PSU_IP_ADDRESS = 'psu-ip-address';
const X_CLIENT_CERTIFICATE = 'x-client-certificate';
//const X_CLIENT_CERTIFICATE_1 = 'clientcert';

//ssl-client-cert

const DATE = 'date';

const value = ''

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor( private loaderService: LoadingService, private notificationService: NotificationService, private router: Router) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(this.loaderService.isLoadingEnabled()){
       this.loaderService.showLoader();
        }
        return next.handle(this.addAuthHeaders(req)).pipe(
            catchError(response => {
                if (response instanceof HttpErrorResponse) {
                  this.notificationService.buildAndShowNotification(response);
                }
          
                return of(response);
          
              }),
              finalize(() => this.loaderService.hideLoader()));
    }

    private addAuthHeaders(req: HttpRequest<any>) {
        return this.addDefaultHeadersAndKey(req);
    }

    private  addDefaultHeadersAndKey(req: HttpRequest<any>): HttpRequest<any> {
        const re = environment.api.resourceName.consent;
        const wn = environment.api.resourceName.wellknown;
        const token = environment.api.resourceName.token;
        const payment = environment.api.resourceName.payment;
        const periodicpayment = environment.api.resourceName.periodicPayment;
        if ((req.method == 'DELETE' && req.url.search(payment) === -1 && req.url.search(periodicpayment) === -1) || (req.url.search(re) === -1 && req.url.search(wn) === -1 && req.url.search(token) === -1 && req.url.search(payment) === -1 && req.url.search(periodicpayment) === -1)) {

            const token: string = sessionStorage.getItem('Authtoken');
            const consentId: string = sessionStorage.getItem('consentId');
            if(token != null && consentId != null){
              
                return req.clone({
               
              
                    headers: req.headers
                     .set(AUTHORIZATION, 'Bearer '+ token)
                                .set(X_SBS_ENV_PORT, environment.api.env_port)
                                .set('consent-id', consentId)
                                .set(X_CLIENT_IP_ADDRESS, '182.218.220.23')
                                .set(X_REQUEST_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                      .set(X_CLIENT_CERTIFICATE,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
               ///       .set(X_CLIENT_CERTIFICATE_1,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
                                .set(X_CLIENT_TRACE_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                                .set(API_KEY, environment.api.key)
                                .set(DATE, new Date().toDateString())
                                .set(PSU_IP_ADDRESS, '182.218.220.23')
                                .set('range', '0-4')
                               
                         
                });    
            }
            else {
           
            return req.clone({
                headers: req.headers
                            .set(X_SBS_ENV_PORT, environment.api.env_port)
                            .set(X_CLIENT_CERTIFICATE,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
                       //     .set(X_CLIENT_CERTIFICATE_1,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
                            .set(X_CLIENT_IP_ADDRESS, '182.218.220.23')
                            .set(X_REQUEST_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                            .set(X_CLIENT_TRACE_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                            .set(API_KEY, environment.api.key)
                            .set(DATE, new Date().toDateString())
                            .set(PSU_IP_ADDRESS, '182.218.220.23')
                    
            });
        }
     } 
           
            else{
            
                return req.clone({
               
              
                    headers: req.headers
                  //  .set(AUTHORIZATION, 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55Q29kZSI6IlNCUyIsImNsaWVudElkIjoiUFNEQkUtQUlTLTEyMzQ1NiIsImNvbm5lY3RvcnNJbmZvcm1hdGlvbiI6eyJjb3JlQmFua2luZ1BsYXRmb3JtVjQiOnsidXNlcklkIjoiMDAwMDAxMTEifX0sInVzZXJfbmFtZSI6InJvaGl0IiwiaXNzIjoic2VydmVyLXVzZXItYXV0aCIsImF1dGhvcml6YXRpb25zIjp7InN0YW5kYXJkIjpbImJlcmxpbi1ncm91cCJdfSwidXNlcklkIjoiMDAwMDAxMTEiLCJjbGllbnRfaWQiOiJQU0RCRS1BSVMtMTIzNDU2IiwicHN1SW5mb3JtYXRpb24iOnsiaWQiOiIwMDAwMDExMSIsIm5hbWUiOiJSb2hpdCBTcml2YXN0YXZhIn0sImJ1aWxkRnJvbVJlZnJlc2hUb2tlbiI6ZmFsc2UsImJ1c2luZXNzRGF0ZSI6IjIwMjAtMDgtMjVUMTk6MTY6MTYuMjY0WiIsInRwcEluZm9ybWF0aW9uIjp7ImlkIjoiNWMxZDA1MmM5YTc1ODQwMDAxMWQ1ZWY4IiwibmFtZSI6IlRQUCBDZXJ0aWZpY2F0ZSB3aXRoIEFJUyByb2xlIiwiYWNjcmVkaXRhdGlvbnMiOlsiQUlTIl19LCJkZWxlZ2F0ZUlkIjoiQWdlbnQ1NSIsInNjb3BlIjpbIkFJUyJdLCJhdXRoZW50aWNhdGlvbkRlY2lzaW9uIjp7ImF1dGhlbnRpY2F0aW9uRGVjaXNpb25Ub2tlbiI6IjVmNDU2MzcyMDUxOGI2MDAwMTBkM2RiZSIsImRlY2lzaW9uRGF0ZSI6IjIwMjAtMDgtMjVUMTI6MDA6MDBaIiwic2lnbmF0dXJlRGF0ZSI6IjIwMjAtMDgtMjVUMTk6MTY6MTMuMDU2WiIsImFjdGlvbk5hbWUiOiJBSVNfY29uc2VudHMtY3JlYXRpb24iLCJhdXRoZW50aWNhdGlvblR5cGUiOiJTVFJPTkciLCJwcm90b2NvbCI6InVzZXJuYW1lcGFzc3dvcmQifSwidGVuYW50SWQiOiJTT1BSQSIsImNuZiI6eyJ4NXQjUzI1NiI6ImM3NzY0OTdkMWNjNjMzOGU5MDcyODQ1MTEzZDQwNjc4ODRjNjkxZDgxZDhmZTAyMmE1OTJlZjc0MTY2YWQ5NWYifSwidXNlclR5cGUiOiJQU1VfQllfVFBQIiwiZXhwIjoxNTk4MzgzODc2LCJqdGkiOiIyZDllZTYzMC04YzE2LTRmOTgtOTk5Mi05NGVhNDgwOGRjNTUiLCJzZWN1cml0eUNoZWNrRGF0YSI6eyJhdXRoVHlwZSI6IkFJUyIsImlkIjoiNWY0NTYzNzIxYTJmMzMwMDAxMTY5NmJmIn19.R5PTlkAlVnv0dw1tihqNyUnDr1Lql9C3L-KyvsOGAf2djkFkCj72wN0rtJy1ubxM_B8ewiOHA-WNAsA6y9WZJLPdtnlf7uVfOpq36OV9WOPoIzLDyb-hyzmDR61QQd6AUW5eJUYLtRUexmJfiqOxxDvx4KFcVpWYb2tztomgS1BTgTUQVXbZ-wCtnOhcMMqgdIoXA4TxKa5y_LteeyafLgSe9_0RpOjah70BUPinknNnSJvJmH8vevfynvBthAWMjpmhNdM6N2uRjL8fa9PKxrKrwbX8GlOTKRcbg7HHz-Muv4PON7H0Ks1gjtxPOt-YQXkQLPhTr7iAKJEM65yBgQ')
                                .set(X_SBS_ENV_PORT, environment.api.env_port)
                                .set(X_CLIENT_CERTIFICATE,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
                           //     .set(X_CLIENT_CERTIFICATE_1,'-----BEGIN CERTIFICATE-----MIIEbDCCA1SgAwIBAgIIY1X3TAHKplowDQYJKoZIhvcNAQELBQAwaTELMAkGA1UEBhMCRlIxDjAMBgNVBAgTBVBhcmlzMQ4wDAYDVQQHEwVQYXJpczEMMAoGA1UEChMDU0JTMQwwCgYDVQQLEwNEeFAxHjAcBgkqhkiG9w0BCQEWD3Jvb3RDQUByb290LmNvbTAeFw0xODEyMjEwODE5MDBaFw0yODEyMTkxODAwMDBaMIGEMQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUGFyaXMxDjAMBgNVBAcTBVBhcmlzMQwwCgYDVQQKEwNTQlMxDDAKBgNVBAsTA0R4UDEeMBwGCSqGSIb3DQEJARYPcm9vdENBQHJvb3QuY29tMRkwFwYDVQRhExBQU0RCRS1BTEwtMTIzNDU2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBBWesD++ZFEe7fBEcx4JBiHl20dh3eSizo6vsQVkjyFhmF1tm2Z8m99wDahcDGl1eQUMXnI97m05ouS1G/s+qy9yoa8CitzymVYIr2gxBT5rGqsPaymyH6DR7v/zf0fjzvfS1FalpVgOS1mpfBWkASBQ/XNyIOv68E5EcFDGN/w9Ry/QTs1puKb/5rew7QwZK8uruzkOUm3VVyaSAcgBwaDysZ6smhwZtpT25gv2Va2ap0KQHISo7jNfl86iopNSMJabZtj276MllCHAnwPz19p5+gLRYU1z0OGOAEE6N/L5wMK8qORco6/aD8wZzmmjWGD9xz/fRPaIel7qwItYwIDAQABo4H7MIH4MAkGA1UdEwQCMAAwHQYDVR0OBBYEFJKABCyIJ49ax5uiPNPaFLZ6xQccMAsGA1UdDwQEAwIC/DB2BggrBgEFBQcBAwRqMGgwZgYGBACBmCcCMFwwTDARBgcEAIGYJwEBDAZQU1BfQVMwEQYHBACBmCcBAgwGUFNQX1BJMBEGBwQAgZgnAQMMBlBTUF9BSTARBgcEAIGYJwEEDAZQU1BfSUMMA05CQgwHQmVsZ2l1bTAUBgNVHSAEDTALMAkGBwQAi+xAAQQwEQYJYIZIAYb4QgEBBAQDAgWgMB4GCWCGSAGG+EIBDQQRFg94Y2EgY2VydGlmaWNhdGUwDQYJKoZIhvcNAQELBQADggEBAECFgUvExG8VUAUJGSyi/SMcgR9jUvMh6igD1CzGy3h0NdkCT1ASIZC10P8KQ0rpe8Kbd1bxog/lprD0bdz1iQO/b/SPPSvjARbaUuPp8WoW3EGePFG6ygE7E2T6SkSq71+LijJzyDZZmZss05GCxc5aFrd2Ox70LmLnk0/FaSJGkjkJqE7rWnoMUnbMnMRxl8K2n1bOWLeFsch2xMkRdQM9r89S1Bmsy7O8ZeRmVz8FymPjDn7hD2qpQb+Byz2r6/855r203gaflddjqslXih8WQnBW8ymIOGFPHNhWJGLdke+mEJNC5Kq898vItz2k1Nv0WEPFGzTemEAs26W61EA=-----END CERTIFICATE-----')
                               .set(X_CLIENT_IP_ADDRESS, '182.218.220.23')
                                .set(X_REQUEST_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                                .set(X_CLIENT_TRACE_ID, '4bb64c99-1862-1c7a-b567-c8243ebe3cb7')
                                .set(API_KEY, environment.api.key)
                                .set(DATE, new Date().toDateString())
                                .set(PSU_IP_ADDRESS, '182.218.220.23')
                               
                         
                });
            }
           
        }


}
