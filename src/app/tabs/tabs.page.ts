import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    
  }


  checkAndRedirect(){
    console.log(this.router.url)
    if(this.router.url === '/menu/consent/tab/accounts'){
      this.router.navigate(['/menu/accounts'])
    }
    else{
      this.router.navigate(['/menu/payments'])
    }
  }
}
