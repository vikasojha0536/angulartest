import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';
import { AccountsResolverGuard } from '../guards/accounts-resolver.guard';
import { DashboardGuard } from '../guards/dashboard.guard';

const routes: Routes = [
  {
    path: 'tab',
    component: TabsPage,
    children: [
      {
        path: 'accounts',
        loadChildren: () => import('../accounts/accounts.module').then(m => m.AccountsPageModule)
        //, resolve: { data: AccountsResolverGuard }, canActivate: [DashboardGuard]
      },
      {
        path: 'payments',
        loadChildren: () => import('../payments/payments.module').then(m => m.PaymentsPageModule)
     //   ,  canActivate: [DashboardGuard]
      },
      {
        path: 'consent',
        loadChildren: () => import('../consents/consents.module').then(m => m.ConsentsPageModule)
     //   ,  canActivate: [DashboardGuard]
      },
      {path: '', redirectTo: 'accounts', pathMatch: 'full'}
    ]
  },
  {
    path: '',
    redirectTo: 'tab/accounts',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {
}
