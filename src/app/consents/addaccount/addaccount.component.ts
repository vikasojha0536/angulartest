import { Component, OnInit } from '@angular/core';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { PopoverController, ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PopOverAccount } from 'src/app/models/account.model';

@Component({
  selector: 'app-addaccount',
  templateUrl: './addaccount.component.html',
  styleUrls: ['./addaccount.component.scss'],
})
export class AddaccountComponent implements OnInit {
  form: FormGroup;
  account: PopOverAccount;
  constructor(public popoverController: PopoverController,private formBuilder: FormBuilder ,public modalController: ModalController,
    private navParams: NavParams) { }

  ngOnInit() {
    this.account = this.navParams.get('account');
    this.initForm(this.account);
  }

  dismiss() {
    this.modalController.dismiss({'account': this.account});
  }

  add(){
    const refId: string = this.form.get('referenceId').value
    if(refId.trim().length != 0){
    this.account = {
      referenceId : refId,
      account: this.form.get('account').value,
      balance: this.form.get('balance').value,
      transaction: this.form.get('transaction').value
    }
  }
    this.modalController.dismiss({'account': this.account});
  }


  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: {name: value},
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  private initForm(acc: PopOverAccount) {
    this.form = this.formBuilder.group({

       referenceId: this.getRefernceId(acc),

       account: this.getCheckbox(acc, 'account'),
      
       balance: this.getCheckbox(acc, 'balance'),
   
       transaction: this.getCheckbox(acc, 'transaction'),
     

    });
  }

getRefernceId(acc: PopOverAccount){
    if(acc == undefined || acc.referenceId == null || acc.referenceId ==""){
        return '';
    }
    return acc.referenceId;
}

getCheckbox(acc: PopOverAccount, val: string){
  if(acc != undefined ){
      if(val == 'account'){
        return acc.account;
      }
      if(val == 'balance'){
        return acc.balance;
      }
      if(val == 'transaction'){
        return acc.transaction;
      }
  }
  return false;
}
}
