import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule } from '@ionic/angular';


import { ConsentPageRoutingModule } from './consents-routing.module';
import { ConsentsPage } from './consents.page';
import { AddaccountComponent } from './addaccount/addaccount.component';
import { AllconsentComponent } from './allconsent/allconsent.component';
import { ParticularComponent } from './particular/particular.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ConsentPageRoutingModule,
  ],
  declarations: [ConsentsPage, AddaccountComponent, AllconsentComponent, ParticularComponent ],
  providers: [InAppBrowser],
  entryComponents: [ AddaccountComponent ]
})
export class ConsentsPageModule {}
