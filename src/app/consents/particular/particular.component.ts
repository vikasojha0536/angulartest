import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PostConsent, ConsentResponse, AccountAccess, AccountReference } from 'src/app/models/consent.model';


import { PopOverAccount } from 'src/app/models/account.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PopoverController, ModalController } from '@ionic/angular';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';


import { SessionService } from 'src/app/services/session.service';
import { AisService } from 'src/app/services/ais.service';
import { AddaccountComponent } from '../addaccount/addaccount.component';

@Component({
  selector: 'app-particular',
  templateUrl: './particular.component.html',
  styleUrls: ['./particular.component.scss'],
})
export class ParticularComponent implements OnInit {
  form: FormGroup;


  consentResponse: ConsentResponse;

  account: AccountReference[];
  balance: AccountReference[] = null;
  transaction: AccountReference[] =null;


  popOverAccount: PopOverAccount[];
  expand: boolean[] =[];

  constructor(private route: ActivatedRoute,
    private aisService: AisService,  private formBuilder: FormBuilder,
    private router: Router, public popoverController: PopoverController,
    public modalController: ModalController,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.initForm();
    this.account = [];
  //  this.balance = [];
  //  this.transaction = [];
    this.popOverAccount = [];

  }

  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      dateValid: null,
      recurringIndicator: false,
      combinedServiceIndicator: false,
      frequencyPerDay: null
      // iban1: 'BE20999000900756',
      // iban2: 'BE09999000900857'
    });
  }

  expandAccount(i : number): boolean{
    return this.expand[i] = !this.expand[i];
  }

  checkSelected(i: number): boolean{
    return this.expand[i];
  }

  deleteAccount(index: number, id: string){
    console.log('index' +index)
     this.popOverAccount.splice(index,1);
     this.account.splice(this.account.findIndex( t => t.iban === id),1)
     this.balance.splice(this.balance.findIndex( t => t.iban === id),1)
     this.transaction.splice(this.transaction.findIndex( t => t.iban === id),1)
     console.log('size '+this.popOverAccount.length)
  }


  async addAccount() {
    const modal = await this.modalController.create({
      component: AddaccountComponent
    });
  
    modal.onDidDismiss().then(d => {
    
      if(d != undefined && d.data != undefined && d.data.account != undefined){
      const acc = d.data.account
      if (d.data.account.account) {
        if(this.balance == null){
          this.balance = [];
          }
          
      
        this.account.push({ iban: acc.referenceId });

      }

      if (d.data.account.balance) {
        if(this.balance == null){
        this.balance = [];
        }
      
        this.balance.push({ iban: acc.referenceId });

      }

      if (d.data.account.transaction) {
        if(this.transaction == null){
        this.transaction = [];
        }
       
        this.transaction.push({ iban: acc.referenceId });

      }
      this.popOverAccount.push(acc)
    }
  });
    
    return await modal.present();
  }


  resetForm(){
    this.form.reset();
    this.popOverAccount= [];
    this.account= [];
    this.balance =[];
    this.transaction = [];
  }
  
  async editAccount(acc?: PopOverAccount, index?:number) {
    const modal = await this.modalController.create({
      component: AddaccountComponent,
      componentProps: { account: acc }
    });
    if(acc != undefined){
      console.log('removed before editing'+acc.referenceId);
    
   // this.account.splice(index,1);
  //  this.balance.splice(index,1);
   // this.transaction.splice(index,1);
    }
    modal.onDidDismiss().then(d => {
      this.popOverAccount.splice(index,1);
      this.account.splice(index, 1);
      this.balance.splice(index, 1);
      this.transaction.splice(index, 1);
      if(d != undefined && d.data != undefined && d.data.account != undefined){
      const acc = d.data.account
      if (d.data.account.account) {
       
          
      
        this.account.push({ iban: acc.referenceId });

      }

      if (d.data.account.balance) {
        if(this.balance == null){
        this.balance = [];
        }
      
        this.balance.push({ iban: acc.referenceId });

      }

      if (d.data.account.transaction) {
        if(this.transaction == null){
        this.transaction = [];
        }
       
        this.transaction.push({ iban: acc.referenceId });

      }
      this.popOverAccount.push(acc)
    }
  });
    
    return await modal.present();
  }


  dismiss() {
    this.form.reset();
    this.popOverAccount =[];
 
    this.router.navigate(['/welcome']);
  }

  createConsent() {
  
    if(this.account.length == 0){
        this.account =null;
    }

    if(this.balance.length == 0){
      this.balance =null;
  }
  if(this.transaction.length == 0){
    this.transaction =null;
}
    const accountAccess: AccountAccess = {
      accounts: this.account,
      balances: this.balance,
      transactions: this.transaction
    }


    const consentToCreate: PostConsent = {
      validUntil: this.form.get('dateValid').value.substr(0, 10),
      combinedServiceIndicator: this.form.get('combinedServiceIndicator').value,
      recurringIndicator: this.form.get('recurringIndicator').value,
      frequencyPerDay: this.form.get('frequencyPerDay').value,
      access: accountAccess
    };

 //   this.form.reset();
  //  this.popOverAccount = [];
 //   this.account = [];
 //   this.balance = [];
  //  this.transaction = [];
    this.aisService.createPost(consentToCreate).subscribe(c => {
      this.consentResponse = c
      const href = this.consentResponse._links.selectAuthenticationMethod.href;
      // console.log('consent id before storing'+ c.consentId);
      //  this.sessionService.storeConsent(c.consentId);
      sessionStorage.removeItem('allaccounts');
      
      this.router.navigate(['authorisation'], {state: {scaMethods: this.consentResponse.scaMethods, href: href, resourceId: c.consentId}});
    });
  }
}
