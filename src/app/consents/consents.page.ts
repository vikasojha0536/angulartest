import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { AisService } from '../services/ais.service';


import { FormBuilder } from '@angular/forms';
import { PopoverController, ModalController, AlertController } from '@ionic/angular';

import { SessionService } from '../services/session.service';


@Component({
  selector: 'app-consent',
  templateUrl: './consents.page.html',
  styleUrls: ['./consents.page.scss']
})
export class ConsentsPage implements OnInit {
  

  constructor(private route: ActivatedRoute,
    private aisService: AisService,  private formBuilder: FormBuilder,
    private router: Router, public popoverController: PopoverController,
    public modalController: ModalController,
    private sessionService: SessionService, public alertController: AlertController) { }

  ngOnInit() {
   

  }

checkAndNavigate(type: string){
  const consentId = this.sessionService.getLastAddedConsentId();
  if(consentId == null || consentId ==undefined){
    if(type === 'particular'){
      this.router.navigate(["/consents/particular"])
    }
    else{
      this.router.navigate(["/consents/allconsent"])
    }
  
  }
  else{
      this.presentAlertConfirm(type);
  }
}


async presentAlertConfirm(type: string)   {
  
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Confirm!',
    message: 'Adding Consent will <strong>invalidate</strong> the previous consent!!!',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        
        }
      }, {
        text: 'Okay',
        handler: () => {
           sessionStorage.removeItem('consentId');
          sessionStorage.removeItem('Authtoken');
          if(type === 'particular'){
            this.router.navigate(["/consents/particular"])
          }
          else{
            this.router.navigate(["/consents/allconsent"])
          }
        }
      }
    ]
  });

  await alert.present();

   


}
}