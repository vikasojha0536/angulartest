

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



import { ConsentsPage } from './consents.page';
import { ParticularComponent } from './particular/particular.component';
import { AllconsentComponent } from './allconsent/allconsent.component';

const routes: Routes = [
  { path: '', component: ConsentsPage},
 
   { path: 'particular', component: ParticularComponent },
   { path: 'allconsent', component: AllconsentComponent   }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsentPageRoutingModule {}

