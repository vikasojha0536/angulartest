import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllconsentComponent } from './allconsent.component';

describe('AllconsentComponent', () => {
  let component: AllconsentComponent;
  let fixture: ComponentFixture<AllconsentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllconsentComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllconsentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
