import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, PickerController, PopoverController, AlertController } from '@ionic/angular';


import { ActivatedRoute } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Consent, PostConsent, AccountAccess } from 'src/app/models/consent.model';
import { AisService } from 'src/app/services/ais.service';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { ConsentsService } from 'src/app/services/consents.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-allconsent',
  templateUrl: './allconsent.component.html',
  styleUrls: ['./allconsent.component.scss'],
})
export class AllconsentComponent implements OnInit {
  form: FormGroup;

  constructor(public modalController: ModalController, private router: Router, private formBuilder: FormBuilder, private aisService: AisService,
    private popoverController: PopoverController,private sessionService: SessionService, private alertController: AlertController) { 
  }

  ngOnInit() {
//     if(this.sessionService.getLastAddedConsentId() !=null && this.sessionService.getLastAddedConsentId() !=undefined){
// this.presentAlertConfirm();
//     }
    this.initForm();
  }



  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Adding another consent will <strong>invalidate</strong> the previous consent!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            sessionStorage.removeItem('consentId');
            sessionStorage.removeItem('Authtoken');
           
          
           
          }
        }
      ]
    });

    await alert.present();
  }


  private initForm() {
    this.form = this.formBuilder.group({
      dateValid: null,
      recurringIndicator: false,
      combinedServiceIndicator: false,
      frequencyPerDay: null,
      access: null
    });
  }

  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  resetForm(){
    this.form.reset();
  }

  dismiss(){
    this.router.navigate(['/welcome']);
  }
  createConsent() {
var accountsAccess: AccountAccess;
    if(this.form.get('access').value === 'availableAccounts'){
      this.sessionService.storeAllAccounts();
       accountsAccess = {
      availableAccounts: "all-accounts"
      }
    }
    else if(this.form.get('access').value === 'availableAccountsWithBalance'){
      accountsAccess = {
        availableAccountsWithBalance: "all-accounts"
        }
    }
    else{
      accountsAccess = {
        allPsd2: "all-accounts"
        }
    }
     
    const consentToCreate: PostConsent = {
      validUntil: this.form.get('dateValid').value.substr(0,10),
      combinedServiceIndicator: this.form.get('combinedServiceIndicator').value,
      recurringIndicator: this.form.get('recurringIndicator').value,
      frequencyPerDay: this.form.get('frequencyPerDay').value,
      access: accountsAccess
    };
   // this.form.reset();

    this.aisService.createPost(consentToCreate).subscribe(c => {
    //  this.sessionService.storeConsent(c.consentId)
      this.router.navigate(['authorisation'], {state: {scaMethods: c.scaMethods, href: c._links.selectAuthenticationMethod.href, resourceId: c.consentId}});
    });
  }
}
