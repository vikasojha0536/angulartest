import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  form: FormGroup;
  inputConfirmType = 'password';
  inputType = 'password';
  constructor(private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.initForm();
  }


  dismiss(){
    this.router.navigate(['/home']);
    
  }

  initForm(): FormGroup {
   
    return this.formBuilder.group({
      dateValid: null,
      email:['', Validators.email],
     username: [null, Validators.required],
     password: null,
     account: false,
     confirmpassword: null

    });
  
}

login(){
  console.log(this.form.errors)
  const name = this.form.get('username').value
      this.router.navigate(['welcome']);

}

}
