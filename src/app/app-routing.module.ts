import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { WelcomeGuard } from './guards/welcome.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'consents', loadChildren: () => import('./consents/consents.module').then(m => m.ConsentsPageModule)},
  {path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)},
  {path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupPageModule)},
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)},
  {path: 'authorisation', loadChildren: () => import('./authorisation/authorisation.module').then(m => m.AuthorisationPageModule)},
 // {path: 'welcome', loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomePageModule), canActivate: [WelcomeGuard, LoginGuard]},
  {path: 'tabs', loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)},

  {
    path: 'payments',
    loadChildren: () => import('./payments/payments.module').then( m => m.PaymentsPageModule)
  },
  // {
  //   path: 'authorisation',
  //   loadChildren: () => import('./authorisation/authorisation.module').then( m => m.AuthorisationPageModule)
  // },
  // {
  //   path: 'menu',
  //   loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  // },
  // {
  //   path: 'consentpopover',
  //   loadChildren: () => import('./consentpopover/consentpopover.module').then( m => m.ConsentpopoverPageModule)
  // },

  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'menuhome',
    loadChildren: () => import('./menuhome/menuhome.module').then( m => m.MenuhomePageModule)
  },
  {
    path: 'paymenttab',
    loadChildren: () => import('./paymenttab/paymenttab.module').then( m => m.PaymenttabPageModule)
  },
  {
    path: 'consenttab',
    loadChildren: () => import('./consenttab/consenttab.module').then( m => m.ConsenttabPageModule)
  },
  {path: '**', redirectTo: 'welcome'},




  

 



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
