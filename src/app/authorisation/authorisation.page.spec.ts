import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuthorisationPage } from './authorisation.page';

describe('AuthorisationPage', () => {
  let component: AuthorisationPage;
  let fixture: ComponentFixture<AuthorisationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorisationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuthorisationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
