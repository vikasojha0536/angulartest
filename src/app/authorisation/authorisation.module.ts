import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AuthorisationPageRoutingModule } from './authorisation-routing.module';

import { AuthorisationPage } from './authorisation.page';

import { TokenComponent } from './token/token.component';

import { PaymentauthorisationComponent } from './paymentauthorisation/paymentauthorisation.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthorisationPageRoutingModule
  ],
  declarations: [AuthorisationPage, TokenComponent, PaymentauthorisationComponent],
  providers: [InAppBrowser]
})
export class AuthorisationPageModule {}
