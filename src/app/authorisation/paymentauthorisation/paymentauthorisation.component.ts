import { Component, OnInit } from '@angular/core';
import { ScaMethod, ScaMethodPatch } from 'src/app/models/consent.model';
import { AisService } from 'src/app/services/ais.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-paymentauthorisation',
  templateUrl: './paymentauthorisation.component.html',
  styleUrls: ['./paymentauthorisation.component.scss'],
})
export class PaymentauthorisationComponent implements OnInit {
  scaMethods : ScaMethod[];
  href: string;
  scope: string;
  resourceId: string;
  paymentType: string;
  mode: string;
  redirect_uri: string;
  code: string;
  constructor( private aisService: AisService, private router: Router, private browser: InAppBrowser, 
    private sessionService: SessionService) { }

  ngOnInit() {
    this.scaMethods = this.router.getCurrentNavigation().extras.state.scaMethods;
    this.href = this.router.getCurrentNavigation().extras.state.href;
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
    this.mode = this.router.getCurrentNavigation().extras.state.mode;
    this.resourceId = this.router.getCurrentNavigation().extras.state.resourceId;
    console.log('got the resource ID' + this.resourceId);
    this.redirect_uri = 'http://localhost:8100/payments/submit/';

  }

  selectAuthorisation(methodId: string) {

    
    var authorisePath: string;

   console.warn('here in authorisationConfirm component')
    const scaMethodPatch: ScaMethodPatch = {
      authenticationMethodId: methodId
    }


  this.aisService.put(scaMethodPatch, this.href).subscribe(c =>
    this.aisService.getMetaData(c._links.scaOAuth.href).subscribe(m => {authorisePath = m.authorization_endpoint



    var url: string = authorisePath+'?response_type=code&redirect_uri='+this.redirect_uri+'&code_challenge=qjrzSW9gMiUgpUvqgEPE4_-8swvyCtfOVvg55o5S_es&&state=state_value_from_tpp&code_challenge_method=S256&ui_locales=en-EN&client_id='+environment.auth.clientId+'&scope=PIS:'+this.resourceId;
    this.sessionService.storePaymentId(this.resourceId);
   this.sessionService.storePaymentType(this.paymentType);
     sessionStorage.setItem('mode',this.mode);
  this.browser.create(url, '_self','clearcache=yes,clearsessioncache=yes');


// this is a hack must be fixed maybe capacitor has a solution
  //   let browserRef = window.open(url, '_blank', 'clearcache=yes,clearsessioncache=yes');
  //   browserRef.addEventListener("loadstart", (event: any) => {
  //     if ((event.url).indexOf('?code=') !== -1) {
  //       this.code = event.url.slice(event.url.indexOf('?code=') + '?code='.length, event.url.indexOf('?code=') + '?code='.length+6);
  //       console.log('code is '+this.code)
  //       // here is your token, now you can close the InAppBrowser

  //       browserRef.close();
  //       this.sessionService.storeConsent(this.resourceId);
  //       this.router.navigate(['/payments/submit'], {state: {code: this.code, paymentType: this.paymentType, mode: this.mode, paymentId: this.resourceId}});
  //     }
  //   });

    
   }));
 
  
}

  // disableButton(methodId: string): boolean{
  //     return methodId != environment.authenticationType;
  // }
}
