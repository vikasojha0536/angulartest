import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorisationPage } from './authorisation.page';

import { TokenComponent } from './token/token.component';
import { AuthorisationTokenResolverGuard } from '../guards/authorisationtoken-resolver.guard';
import { PaymentGuard } from '../guards/payment.guard';

import { SkeletonGuard } from '../guards/skeleton.guard';
import { PaymentauthorisationComponent } from './paymentauthorisation/paymentauthorisation.component';

const routes: Routes = [

 // { path: 'token', component: TokenComponent, resolve: { AuthorisationTokenResolverGuard }, canActivate: [SkeletonGuard] },
  { path: 'token', component: TokenComponent},

  { path: 'paymentauthorisation', component: PaymentauthorisationComponent},

  {
    path: '',
    component: AuthorisationPage,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthorisationPageRoutingModule {}
