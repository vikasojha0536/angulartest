import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  ScaMethod, ScaMethodPatch } from '../models/consent.model';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AisService } from '../services/ais.service';
import { environment } from '../../environments/environment';
import { ConsentsService } from '../services/consents.service';
import { SessionService } from '../services/session.service';
import { LoadingService } from '../services/loading.service';
//import { WebView } from '@ionic-native/ionic-webview/ngx';



@Component({
  selector: 'app-authorisation',
  templateUrl: './authorisation.page.html',
  styleUrls: ['./authorisation.page.scss'],
})
export class AuthorisationPage implements OnInit {
  scaMethods : ScaMethod[];
  href: string;
  scope: string;
  resourceId: string;
  paymentType: string;
  mode: string;
  redirect_uri: string;
  code: string;
  constructor( private aisService: AisService, private router: Router, private browser: InAppBrowser, 
    private sessionService: SessionService, private loader: LoadingService) { }

  ngOnInit() {
    this.scaMethods = this.router.getCurrentNavigation().extras.state.scaMethods;
    this.href = this.router.getCurrentNavigation().extras.state.href;
    this.resourceId = this.router.getCurrentNavigation().extras.state.resourceId;
    console.log('got the resource ID' + this.resourceId);
    this.redirect_uri = 'http://localhost:8100/authorisation/token/';

  }

  selectAuthorisation(methodId: string) {

    
    var authorisePath: string;

   console.warn('here in authorisationConfirm component')
    const scaMethodPatch: ScaMethodPatch = {
      authenticationMethodId: methodId
    }


  this.aisService.put(scaMethodPatch, this.href).subscribe(c =>
    this.aisService.getMetaData(c._links.scaOAuth.href).subscribe(m => {authorisePath = m.authorization_endpoint



    var url: string = authorisePath+'?response_type=code&redirect_uri='+this.redirect_uri+'&code_challenge=qjrzSW9gMiUgpUvqgEPE4_-8swvyCtfOVvg55o5S_es&&state=state_value_from_tpp&code_challenge_method=S256&ui_locales=en-EN&client_id='+environment.auth.clientId+'&scope=AIS:'+this.resourceId;
    this.sessionService.storeConsent(this.resourceId);
    this.loader.disableLoading();
  this.browser.create(url, '_self','clearcache=yes,clearsessioncache=yes');


// this is a hack must be fixed maybe capacitor has a solution
    // let browserRef = window.open(url, '_blank', 'clearcache=yes,clearsessioncache=yes');
    // browserRef.addEventListener("loadstart", (event: any) => {
    //   if ((event.url).indexOf('?code=') !== -1) {
    //     this.code = event.url.slice(event.url.indexOf('?code=') + '?code='.length, event.url.indexOf('?code=') + '?code='.length+6);
    //     console.log('code is '+this.code)
    //     // here is your token, now you can close the InAppBrowser

    //     browserRef.close();
    //     this.sessionService.storeConsent(this.resourceId);
    //     this.router.navigate(['/authorisation/token'], {state: {code: this.code}});
    //   }
    // });

    
  }));
 
  }

  // disableButton(methodId: string): boolean{
  //     return methodId != environment.authenticationType;
  // }
}
