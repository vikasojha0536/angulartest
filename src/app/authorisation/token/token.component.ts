import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AisService } from 'src/app/services/ais.service';
import { LoadingController } from '@ionic/angular';
import { finalize, mergeMap, tap } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.scss'],
})
export class TokenComponent implements OnInit {

  numbers: number[];
  loading: any;

  constructor(private route: ActivatedRoute, private router: Router, public loadingController: LoadingController, private accountService: AisService
    , private sessionService: SessionService) { }

  async ngOnInit() {

    var code = this.route.snapshot.queryParams['code'];

    const extraState = this.router.getCurrentNavigation().extras.state
    if(extraState != undefined){
      code = extraState.code;
    }
    
    console.log('here in token point')
   //  await this.presentLoading();


    this.numbers = Array(10).fill(0).map((x, i) => i);

    const body = new HttpParams()
    .set('client_id', environment.auth.clientId)
    .set('code', code)
    .set('code_verifier', 'M25iVXpKU3puUjFaYWg3T1NDTDQtcW1ROUY5YXlwalNoc0hhakxifmZHag')
    .set('grant_type', 'authorization_code')
    .set('redirect_uri', 'http://localhost:8100/authorisation/token/')
  const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')




  return this.accountService.token(body.toString(), headers)
    .pipe(
     
      //  tap(s =>
      //   this.loading.hideLoader()

      //  ),
      mergeMap(t => this.accountService.refreshToken(t.refresh_token, headers)
        .pipe(
          tap(s =>
            this.sessionService.storeAccountToken(s.access_token)
          ))))   .subscribe(c => {
            localStorage.setItem('dashboard', 'refresh');
            this.router.navigateByUrl('/tabs/tab/accounts');
          });
   from(this.numbers)
        .pipe(finalize(async () => {
         this.loading.dismiss();
          localStorage.setItem('dashboard', 'refresh');
          this.router.navigateByUrl('/menu/accounts');
        }))
       .subscribe(c => {
          localStorage.setItem('dashboard', 'refresh');
          this.router.navigateByUrl('/menu/accounts');
        });
  }


  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Fetching account information...' });
    await this.loading.present();
  }

}