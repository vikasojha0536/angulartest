export interface Consent {
  id?:           string;
  aspspId:      string;
  psuId:        string;
  validityDate: Date;
  subjects:     Subject[];
}

export interface ConsentResponse {
  consentId:           string;
  consentStatus:      string;
  _links:        ConsentLinks;
  scaMethods: ScaMethod[];
}


export interface CancelAuthResponse {
  authorisationId:           string;
  scaStatus:      string;
  _links:        ConsentLinks;
  scaMethods: ScaMethod[];
}
export interface ConsentLinks {
  self:        Link;
  scaOAuth: Link;
  status: Link;
  selectAuthenticationMethod: Link;
  scaStatus: Link;
  startAuthorisation?: Link;
}


export interface Link {
  href: string;
  
}

export interface ScaMethod {
  authenticationType: string;
  authenticationVersion: string;
  authenticationMethodId: string;
  name: string;
  explanation: string;
  
}

export interface ScaMethodPatch {

  authenticationMethodId: string;

  
}

export interface Subject {
  iban:        string;
  permissions: string[];
}



export interface PostConsent {
  validUntil:           Date;
  frequencyPerDay:      Number;
  recurringIndicator:        boolean;
  combinedServiceIndicator: boolean;
  access?:     AccountAccess;
}

export interface AccountAccess {
  accounts?:        AccountReference[];
  balances?: AccountReference[];
  transactions?: AccountReference[];
  availableAccounts?: string;
  availableAccountsWithBalance?: string;
  allPsd2?: string;
}

export interface AccountReference {
  iban : String
}