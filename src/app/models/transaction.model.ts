import { Link } from './consent.model';
import { Amount } from './balance.model';



export interface BGBalanceTransactions {
  transactions: Transactions;
  balances: Balance[];
  _links: Link;
 
}

export interface Balance {
  balanceAmount?: Amount;
  balanceType?: string;
  referenceDate?: Date;
  lastCommittedTransaction?: string;
 
}



export interface Transactions {
  booked: BgTransaction[];
  pending: BgTransaction[];
  _links: LinkAccount;
 
}

export interface LinkAccount {
  first?: Link;
  last?: string;
  previous?: string;
  next?: string;
  account?: string;
  self?: string;
  scaOAuth?: string;
}




export interface AccountReference {
  iban?: string;
  bban?: string;
  pan?: string;
  maskedPan?: string;
  msisdn?: string;
  currency? : string;
  
}

export interface BgTransaction {
  transactionId?: string;
  bookingDate?: Date;
  valueDate?: Date;
  transactionAmount?: Amount;
  creditorAccount?: string;
  debtorAccount: string;
  remittanceInformationUnstructured?: string;
  remittanceInformationStructured?: string;
  creditorName?: string;
  debtorName?: string;
  proprietaryBankTransactionCode?: string;
  entryReference?: string;
  _links?: Link;
}






export enum BookingStatus {
  BOOKED = 'BOOKED',
  PENDING = 'PENDING',
  BOTH = 'BOTH'
}


