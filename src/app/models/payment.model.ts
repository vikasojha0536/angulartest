import { Amount } from './balance.model';
import { AccountReference } from './transaction.model';
import { ConsentLinks, ScaMethod } from './consent.model';




export interface BgPayment {
  transactionStatus?: string,
  endToEndIdentification?: string;
  instructedAmount?:       Amount;
  creditorAccount?:    AccountReference;
  debtorAccount?:      AccountReference;
  creditorAddress?:      CreditorAddress;
  creditorName?:      string;
  remittanceInformationUnstructured?:  RemittanceInformation;
  requestedExecutionDate?: Date;
  currencyOfTransfer?: string;
  frequency?: string,
  startDate?: Date,
  dayOfExecution?: string;
  executionRule?: string;
  enddate?: Date;

}

export interface PaymentResponse {
  paymentId:           string;
  consentStatus:      string;
  _links:        ConsentLinks;
  scaMethods: ScaMethod[];
}

export interface PaymentDeleteResponse {
  transactionStatus:           string;

  _links:        ConsentLinks;

}

export interface CreditorAddress {
  buildingNumber? : string,
  townName? : string,
  postCode? : string,

  country: string,
  streetName: string,

}

export interface RemittanceInformation {
  structured: string;
}