
export interface Token {
  access_token?: string,
  expires_in?: string,
  token_type?: string,
  refresh_token?: string,
  scope?: string,
  jti?: string,
}


export interface Metadata {
  authorization_endpoint?: string,
  token_endpoint?: string,

}