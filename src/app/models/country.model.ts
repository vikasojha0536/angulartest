export interface Country {
  code: string;
  label: string;
  icon: string;
}

export const COUNTRIES: Country[] = [
  { code: 'BE', label: 'Belgium', icon: 'belgium.svg' },
  { code: 'FR', label: 'France', icon: 'france.svg' },
  { code: 'DE', label: 'Germany', icon: 'germany.svg' },
]
