export interface NotificationAlert {
  referenceId?: string;
  title?: string;
  type: NotificationType;
  code: any;
  label: string;
  message: string;
}

export enum NotificationType {
  ERROR = 'ERROR',
  WARNING = 'WARNING',
  SUCCESS = 'SUCCESS',
  INFO = 'INFO'
}
