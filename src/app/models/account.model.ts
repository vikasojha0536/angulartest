import { Amount } from './balance.model';
import { Link ,ConsentLinks} from './consent.model';



export interface BGAccounts {
  accounts: BGAccount[]
}

export interface PopOverAccount {
  referenceId: string;
  account: boolean;
  balance: boolean;
  transaction: boolean;
}

export interface BGAccount {
  resourceId?: string,
  iban?: string,
  bban?: string,
  pan?: string,
  maskedPan?: string,
  msisdn?: string,
  currency?: string,
  name?: string,
  product?: string,
  bic?: string,
  balances?: BgBalance[];
  cashAccountType?: string,
  _links?: BgLinks
}

export interface BgLinks {
  balances?: Link
  transactions?: Link
}

export interface BgBalance {
  balanceAmount?: Amount,
  balanceType?: string,
  lastChangeDateTime?: Date,
  referenceDate?: Date,
  lastCommittedTransaction?: string
}




export interface scaPatchResponse{
  scaStatus: String,
  _links:        ConsentLinks; 
}