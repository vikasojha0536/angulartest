export enum Role {
  viewer = 'viewer',
  tppManager = 'tpp-manager',
  admin = 'admin',
}
