import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MenuhomePage } from '../menuhome/menuhome.page';
import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/menuhome',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children : [
      {
        path: 'menuhome',
        
        loadChildren: () => import('../menuhome/menuhome.module').then( m => m.MenuhomePageModule)
      
      }
      ,
{
  path: 'accounts',


  loadChildren: () => import('../tabs/tabs.module').then(m => m.TabsPageModule)

}
,{
  path: 'payments',
  
  loadChildren: () => import('../tabs/tabs.module').then( m => m.TabsPageModule)

}
,{
  path: 'consent',
  loadChildren: () => import('../tabs/tabs.module').then( m => m.TabsPageModule)
}
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
