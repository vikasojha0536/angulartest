import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
  
    {
      title: 'Home',
      url: '/menu/menuhome',
      icon: 'home'
    },
    {
      title: 'Account Initiation Service',
      url: '/menu/accounts',
      icon: 'wallet-outline'
    },
    {
      title: 'Payment Initiation Service',
      url: '/menu/payments/tab/payments',
      icon: 'cash-outline'
    },
    {
      title: 'Add Consent',
      url: '/menu/consent/tab/consent',
      icon: 'document-text-outline'
    }
  ]

  selectedPath = '';
  constructor(private router: Router) {

    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
       // console.log('selected path'+ this.selectedPath)
       // console.log('event url'+ event.url)
        this.selectedPath = event.url
      }
    });


  }

  ngOnInit() {
  }

}
