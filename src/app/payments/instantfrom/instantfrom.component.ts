import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { PopoverController } from '@ionic/angular';
import { Amount } from 'src/app/models/balance.model';
import { AccountReference } from 'src/app/models/transaction.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BgPayment } from 'src/app/models/payment.model';
import { BGAccount, BGAccounts } from 'src/app/models/account.model';

@Component({
  selector: 'app-instantfrom',
  templateUrl: './instantfrom.component.html',
  styleUrls: ['./instantfrom.component.scss'],
})
export class InstantfromComponent implements OnInit {
  instaForm: FormGroup;
  amount: string;
  selectedAccount: BGAccount = null;
  bgAccounts: BGAccounts;
  allaccount = sessionStorage.getItem('allaccounts')
  ibanSelected: any = null;
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, public popoverController: PopoverController, private router: Router) { }

  ngOnInit() {
    this.instaForm = this.initInstaForm();
    this.amount = this.router.getCurrentNavigation().extras.state.amount;
    this.route.data.subscribe(d => {
      this.bgAccounts = d.data;
    });

  }


  selectAccount(account: BGAccount) {
    if( this.selectedAccount != null && account.resourceId == this.selectedAccount.resourceId){
     console.log('in the null '+this.selectedAccount)
     this.selectedAccount = null;
   }
   else{
     this.selectedAccount = account;
     console.log(this.selectedAccount);
    }
  


   
 }


   
 resetForm(){
  this.instaForm.reset();
}


 accountSelected(){
  return this.selectedAccount != null;
}

  private initInstaForm(): FormGroup {
    return this.formBuilder.group({
      instaaccount: null,
      selectediban: null
    });
  }

  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  forwardInstaPayment() {

    var debtorAcc = null;
    if(this.selectedAccount != null){
      debtorAcc = this.selectedAccount.iban;
    }
    else if(this.ibanSelected != null){
      debtorAcc = this.ibanSelected
    }
    else{
      debtorAcc = this.instaForm.controls.instaaccount.value;
    }
    const amnt : Amount = {
      amount: this.amount.toString(),
      currency : 'EUR'
    }

    const debtAccount : AccountReference = {
      iban: debtorAcc
    }

    const payment : BgPayment= {
   
    instructedAmount: amnt,
    debtorAccount: debtAccount
    }
    this.router.navigate([ 'payments', 'payment-to'], {state: { payment: payment, paymentType: 'INSTANT PAYMENT'}});
  }


  ibanSelect( event: CustomEvent){
  //  console.log('ibanSelect '+this.ibanSelected)
   this.ibanSelected = event.detail.value
  }

  isIbanSelected( ){
    //console.log('iban selected '+this.ibanSelected)
    return this.ibanSelected != null
   }
}
