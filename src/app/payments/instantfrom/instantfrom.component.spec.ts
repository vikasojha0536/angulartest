import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InstantfromComponent } from './instantfrom.component';

describe('InstantfromComponent', () => {
  let component: InstantfromComponent;
  let fixture: ComponentFixture<InstantfromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantfromComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InstantfromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
