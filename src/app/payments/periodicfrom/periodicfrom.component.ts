import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { PopoverController } from '@ionic/angular';
import { Amount } from 'src/app/models/balance.model';
import { AccountReference } from 'src/app/models/transaction.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BgPayment } from 'src/app/models/payment.model';
import { BGAccount, BGAccounts } from 'src/app/models/account.model';

@Component({
  selector: 'app-periodicfrom',
  templateUrl: './periodicfrom.component.html',
  styleUrls: ['./periodicfrom.component.scss'],
})
export class PeriodicfromComponent implements OnInit {
  periodicForm: FormGroup;
  amount: string;

  payment: BgPayment;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, public popoverController: PopoverController, private router: Router) { }

  ngOnInit() {
    console.log('here')
    this.periodicForm = this.initPeriodicForm();
    
    this.payment = this.router.getCurrentNavigation().extras.state.payment;


  }


  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  forwardPeriodicPayment() {
var date: any;
  if(this.periodicForm.controls.enddate.value != null && this.periodicForm.controls.enddate.value != undefined){
date = this.periodicForm.controls.enddate.value.substr(0,10)
  }

  

    const payment : BgPayment = {
    
    instructedAmount: this.payment.instructedAmount,
    debtorAccount: this.payment.debtorAccount,
    creditorName: this.payment.creditorName,
    creditorAddress: this.payment.creditorAddress,
    creditorAccount: this.payment.creditorAccount,
    frequency: this.periodicForm.controls.frequency.value,
    startDate: this.periodicForm.controls.startdate.value.substr(0,10),
    dayOfExecution: this.periodicForm.controls.dayOfExecution.value,
    executionRule: this.periodicForm.controls.executionRule.value,
    enddate: date
    }
    this.router.navigate([ 'payments', 'payment-resume'], {state: { payment: payment, paymentType: 'STANDING ORDER PAYMENT'}});
  }


  
  resetForm(){
    this.periodicForm.reset();
  }



  private initPeriodicForm(): FormGroup {
    return this.formBuilder.group({
      periodicaccount: null,
      frequency:null,
      periodicexecutiondate:null,
      startdate: null,
      dayOfExecution: null,
      executionRule: null,
      enddate: null,
      selectediban: null
    });
  }
}
