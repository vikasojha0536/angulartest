import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SepastatusComponent } from './sepastatus.component';

describe('SepastatusComponent', () => {
  let component: SepastatusComponent;
  let fixture: ComponentFixture<SepastatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepastatusComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SepastatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
