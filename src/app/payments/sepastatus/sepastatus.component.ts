import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sepastatus',
  templateUrl: './sepastatus.component.html',
  styleUrls: ['./sepastatus.component.scss'],
})
export class SepastatusComponent implements OnInit {

  paymentId: string;
  paymentStatus: string
  constructor(private router: Router) { }

  ngOnInit() {
    this.paymentId = this.router.getCurrentNavigation().extras.state.paymentId;
    this.paymentStatus = this.router.getCurrentNavigation().extras.state.paymentStatus;

  }

  checkAnotherDetails(){
  this.router.navigate(['/menu/payments/tab/payments']);
  }
}
