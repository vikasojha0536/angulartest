import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



import { BgPayment } from '../../models/payment.model';
import { BGAccount } from '../../models/account.model';


@Component({
  selector: 'app-payment-to',
  templateUrl: './payment-to.component.html',
  styleUrls: ['./payment-to.component.scss'],
})
export class PaymentToComponent implements OnInit {


  amount: string;

  bgAccount: BGAccount;
  payment: BgPayment;
  form: FormGroup;
  paymentType: string;

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    console.warn('in the ngOnInit oayament to')
 
    this.amount = this.router.getCurrentNavigation().extras.state.amount;
    this.bgAccount = this.router.getCurrentNavigation().extras.state.account;
    this.payment = this.router.getCurrentNavigation().extras.state.payment;
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
    console.warn('in the ngOnInit account '+this.paymentType)
    this.form = this.initForm();
  }

  nextStep() {
    
console.log('day of execution '+this.payment.dayOfExecution)
    const payment: BgPayment = {


    
    endToEndIdentification: 'END TO END ID',
    creditorName: this.form.controls.party.value.name,
      instructedAmount: {
        amount: this.payment.instructedAmount.amount,
        currency: this.payment.instructedAmount.currency
      },
      creditorAccount: {
  
            iban: this.form.controls.bgAccount.value,
         
          },
          creditorAddress: {
country: this.form.controls.party.value.postalAddress.countryCode,
buildingNumber: this.form.controls.party.value.postalAddress.buildingNumber,
townName: this.form.controls.party.value.postalAddress.city,
postCode: this.form.controls.party.value.postalAddress.postalCode,
streetName: this.form.controls.party.value.postalAddress.street
          },
          debtorAccount: this.payment.debtorAccount,
          startDate: this.payment.startDate,
          frequency: this.payment.frequency,
          currencyOfTransfer: this.payment.currencyOfTransfer,
          requestedExecutionDate: this.payment.requestedExecutionDate,
          dayOfExecution: this.payment.dayOfExecution,
          executionRule: this.payment.executionRule,
          enddate: this.payment.enddate
    }

     if(this.paymentType === 'SEPA PAYMENT'){
      this.router.navigate(['payments', 'payment-from'], {state: {payment: payment}});
    }
    if(this.paymentType === 'INTERNATIONAL PAYMENT'){
      this.router.navigate([ 'payments', 'internationalfrom'], {state: {payment: payment}});
    }
    if(this.paymentType === 'INSTANT PAYMENT'){
      this.router.navigate([ 'payments', 'payment-resume'], {state: {payment: payment, paymentType: this.paymentType}});
    }
    if(this.paymentType === 'STANDING ORDER PAYMENT'){
      this.router.navigate([ 'payments', 'periodicfrom'], {state: {payment: payment}});
    }
  // this.router.navigate(['/tabs', 'tab', 'payments', 'payment-resume'], {state: {payment: payment, paymentType: this.paymentType}});
  }

  resetForm(){
    this.form.reset();
  }

  private initForm(): FormGroup {
    return this.formBuilder.group({
      party: this.formBuilder.group({
        name: [ null, [ Validators.required ] ],
        postalAddress: this.formBuilder.group({
          countryCode: null,
          street: null,
          buildingNumber: null,
          city: null,
          postalCode: null ,
        })
      }),
      bgAccount:  [ null, [ Validators.required ] ]
    });
  }
}
