import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountsResolverGuard } from '../guards/accounts-resolver.guard';
import { DashboardGuard } from '../guards/dashboard.guard';
import { PaymentFromComponent } from './payment-from/payment-from.component';
import { PaymentResumeComponent } from './payment-resume/payment-resume.component';
import { PaymentToComponent } from './payment-to/payment-to.component';

import { PaymentsPage } from './payments.page';

import { PeriodicfromComponent } from './periodicfrom/periodicfrom.component';
import { InternationalfromComponent } from './internationalfrom/internationalfrom.component';
import { InstantfromComponent } from './instantfrom/instantfrom.component';
import { LoginGuard } from '../guards/login.guard';
import { SepaComponent } from './sepa/sepa.component';
import { SepadetailsComponent } from './sepadetails/sepadetails.component';
import { SepacancelComponent } from './sepacancel/sepacancel.component';
import { SepastatusComponent } from './sepastatus/sepastatus.component';
import { SepafromComponent } from './sepafrom/sepafrom.component';
import { SubmitComponent } from './submit/submit.component';
import { PaymenttokenGuard } from '../guards/paymenttoken.guard';


const routes: Routes = [
  { path: '', component: PaymentsPage, canActivate: [LoginGuard] },
  { path: 'payment-from', component: PaymentFromComponent },
  { path: 'payment-to', component: PaymentToComponent },
  { path: 'payment-resume', component: PaymentResumeComponent },
  { path: 'sepa', component: SepaComponent },
  { path: 'sepadetails', component: SepadetailsComponent },
  { path: 'sepacancel', component: SepacancelComponent },
  { path: 'sepastatus', component: SepastatusComponent },
  { path: 'periodicfrom', component: PeriodicfromComponent },
  { path: 'internationalfrom', component: InternationalfromComponent},
  { path: 'instantfrom', component: InstantfromComponent},
  { path: 'submit', component: SubmitComponent ,resolve: { data: PaymenttokenGuard }},
  { path: 'sepafrom', component: SepafromComponent, resolve: { data: AccountsResolverGuard }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentsPageRoutingModule {}
