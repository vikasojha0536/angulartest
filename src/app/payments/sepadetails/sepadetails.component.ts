import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sepadetails',
  templateUrl: './sepadetails.component.html',
  styleUrls: ['./sepadetails.component.scss'],
})
export class SepadetailsComponent implements OnInit {

payment: any;
  constructor(private router: Router) { }

  ngOnInit() {
    this.payment = this.router.getCurrentNavigation().extras.state.payment;
   

  }

  checkAnotherDetails(){
  this.router.navigate(['/menu/payments/tab/payments']);
  }
}
