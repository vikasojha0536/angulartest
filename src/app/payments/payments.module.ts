import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicModule } from '@ionic/angular';

import { NgxCurrencyModule } from 'ngx-currency';
import { PaymentFromComponent } from './payment-from/payment-from.component';
import { PaymentResumeComponent } from './payment-resume/payment-resume.component';
import { PaymentToComponent } from './payment-to/payment-to.component';
import { PaymentsPageRoutingModule } from './payments-routing.module';
import { PaymentsPage } from './payments.page';

import { InstantfromComponent } from './instantfrom/instantfrom.component';
import { InternationalfromComponent } from './internationalfrom/internationalfrom.component';
import { PeriodicfromComponent } from './periodicfrom/periodicfrom.component';
import { SepaComponent } from './sepa/sepa.component';
import { SepacancelComponent } from './sepacancel/sepacancel.component';
import { SepadetailsComponent } from './sepadetails/sepadetails.component';
import { SepastatusComponent } from './sepastatus/sepastatus.component';
import { SepafromComponent } from './sepafrom/sepafrom.component';
import { SubmitComponent } from './submit/submit.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PaymentsPageRoutingModule,
    NgxCurrencyModule
  ],
  declarations: [PaymentsPage, PaymentFromComponent, PaymentToComponent, PaymentResumeComponent, 
     InstantfromComponent, InternationalfromComponent, PeriodicfromComponent, SepaComponent, SepacancelComponent,
      SepadetailsComponent, SepastatusComponent, SepafromComponent, SubmitComponent
  ],
  providers: [InAppBrowser, NgxCurrencyModule]
})
export class PaymentsPageModule {}
