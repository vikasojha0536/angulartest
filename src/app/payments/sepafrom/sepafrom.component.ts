import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BGAccount, BGAccounts } from 'src/app/models/account.model';
import { AccountReference } from 'src/app/models/transaction.model';
import { Amount } from 'src/app/models/balance.model';
import { BgPayment } from 'src/app/models/payment.model';

@Component({
  selector: 'app-sepafrom',
  templateUrl: './sepafrom.component.html',
  styleUrls: ['./sepafrom.component.scss'],
})
export class SepafromComponent implements OnInit {
  paymentType: string;
  form: FormGroup;

  amount: string;
  selectedAccount: BGAccount;
  bgAccounts: BGAccounts;
  allaccount = sessionStorage.getItem('allaccounts')
  ibanSelected: BGAccount = null;
  
  constructor(private router: Router, private formBuilder: FormBuilder,private route: ActivatedRoute) { }

  ngOnInit() {
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
    console.log('from me'+this.paymentType)
    this.form = this.initForm();
    this.amount = this.router.getCurrentNavigation().extras.state.amount;
    this.route.data.subscribe(d => {
      this.bgAccounts = d.data;
    });

  }




      private initForm(): FormGroup {
        return this.formBuilder.group({
          periodicaccount: null,
          selectediban: null 
        });
      }

      ibanSelect( event: CustomEvent){
       // console.log('ibanSelect '+event.detail.value)
       this.ibanSelected = this.bgAccounts.accounts.find(v => v.iban ===  event.detail.value);
      // console.log('ibanSelect '+this.ibanSelected)
      }
    
      isIbanSelected( ){
       // console.log('iban selected '+this.ibanSelected)
        return this.ibanSelected != null
       }

       
         
//   selectAccount(account: BGAccount) {
//     if( this.selectedAccount != null && account.resourceId == this.selectedAccount.resourceId){
//   //   console.log('in the null '+this.selectedAccount)
//      this.selectedAccount = null;
//    }
//    else{
//      this.selectedAccount = account;
// //     console.log(this.selectedAccount);
//     }
  


   
//  }

//  accountSelected(){
//   return this.selectedAccount != null;
// }

resetForm(){
  this.form.reset();
}
      
proceed() {

    var debtorAcc = null;
   
    if(this.ibanSelected != null){
     // console.log('selected iban fir'+this.form.controls.periodicaccount.value.iban)
      debtorAcc = this.ibanSelected.iban
    }
    else{
      console.log('selected iban'+this.form.controls.periodicaccount.value)
      debtorAcc = this.form.controls.periodicaccount.value;
    }
  
    const amnt : Amount = {
      amount: this.amount.toString(),
      currency : 'EUR'
    }


    const debtAccount : AccountReference = {
      iban: debtorAcc
    }


    const payment: BgPayment = {
      instructedAmount: amnt,
    debtorAccount: debtAccount,
   
    }
 
      this.router.navigate(['payments', 'payment-to'], {state: {payment: payment, paymentType: this.paymentType}});

  }
}
