import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SepafromComponent } from './sepafrom.component';

describe('SepafromComponent', () => {
  let component: SepafromComponent;
  let fixture: ComponentFixture<SepafromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepafromComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SepafromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
