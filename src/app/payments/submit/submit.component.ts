import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.scss'],
})
export class SubmitComponent implements OnInit {
  
paymentType: string;
mode: string;
paymentId: string;
  deletepayment: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
    if(this.router.getCurrentNavigation().extras.state != undefined){
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
    this.mode = this.router.getCurrentNavigation().extras.state.mode;
    this.paymentId = this.router.getCurrentNavigation().extras.state.paymentId;
    }
    this.paymentType = sessionStorage.getItem('paymentType')
    this.paymentId = sessionStorage.getItem('paymentId')
 
   
    this.mode = sessionStorage.getItem('mode')
    if(this.mode === 'delete'){
      this.deletepayment = true;
    }

  }



checkDetails(){
  this.router.navigate(['payments', 'sepa'], {state: {paymentType: this.paymentType, selectedSegment: 'DETAILS'}})
}

}
