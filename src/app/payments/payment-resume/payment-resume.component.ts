import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { BgPayment,PaymentResponse } from '../../models/payment.model';
import { PisService } from '../../services/pis.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-payment-resume',
  templateUrl: './payment-resume.component.html',
  styleUrls: ['./payment-resume.component.scss'],
})
export class PaymentResumeComponent implements OnInit {

  payment: BgPayment;
  paymentResponse: PaymentResponse;
  paymentType: string;
  constructor(private pisService: PisService, private router: Router, private sessionService: SessionService) { }

  ngOnInit() {
    this.payment = this.router.getCurrentNavigation().extras.state.payment;
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
  }

  submitPayment() {
    this.pisService.bgCreate(this.payment, this.paymentType).subscribe( p => {
      console.warn('payment posted')
      console.warn(p)
      this.paymentResponse = p
      const href = this.paymentResponse._links.selectAuthenticationMethod.href;
      
      this.router.navigate(['authorisation/paymentauthorisation'], {state: {scaMethods: this.paymentResponse.scaMethods, href: href, paymentType: this.paymentType, mode: 'create', resourceId: p.paymentId}});
    });   
  }
}
