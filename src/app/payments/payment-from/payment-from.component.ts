import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BgPayment } from 'src/app/models/payment.model';


import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { PopoverController } from '@ionic/angular';



@Component({
  selector: 'app-payment-from',
  templateUrl: './payment-from.component.html',
  styleUrls: ['./payment-from.component.scss'],
})
export class PaymentFromComponent implements OnInit {

  sepaForm: FormGroup;

  periodicForm: FormGroup;

  payment: BgPayment;
 
  constructor(private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,public popoverController: PopoverController) {
  }

  ngOnInit() {
    this.payment = this.router.getCurrentNavigation().extras.state.payment;
    this.sepaForm = this.initSepaForm();

  }

  resetForm(){
    this.sepaForm.reset();
  }


  forwardSepaPayment() {




    
    this.payment = {
    requestedExecutionDate: this.sepaForm.controls.sepaexecutiondate.value.substr(0,10),
    instructedAmount: this.payment.instructedAmount,
    debtorAccount: this.payment.debtorAccount,
    creditorName: this.payment.creditorName,
    creditorAddress: this.payment.creditorAddress,
    creditorAccount: this.payment.creditorAccount,

    }
  
    this.router.navigate([ 'payments', 'payment-resume'], {state: { payment: this.payment, paymentType: 'SEPA PAYMENT'}});
  }




  private initSepaForm(): FormGroup {
    return this.formBuilder.group({
    
      sepaexecutiondate: '',

    });
  }

 


  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}
