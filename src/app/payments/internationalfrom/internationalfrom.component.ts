import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ConsentpopoverPage } from 'src/app/consentpopover/consentpopover.page';
import { PopoverController } from '@ionic/angular';
import { Amount } from 'src/app/models/balance.model';
import { AccountReference } from 'src/app/models/transaction.model';
import { Router, ActivatedRoute } from '@angular/router';
import { BgPayment } from 'src/app/models/payment.model';
import { BGAccount, BGAccounts } from 'src/app/models/account.model';

@Component({
  selector: 'app-internationalfrom',
  templateUrl: './internationalfrom.component.html',
  styleUrls: ['./internationalfrom.component.scss'],
})
export class InternationalfromComponent implements OnInit {
  internationalForm: FormGroup;

  selectedAccount: BGAccount;
  payment: BgPayment;

  allaccount = sessionStorage.getItem('allaccounts')


  constructor(private route: ActivatedRoute,private formBuilder: FormBuilder, public popoverController: PopoverController, private router: Router) { }

  ngOnInit() {
    this.internationalForm = this.initInternationalForm();
    this.payment = this.router.getCurrentNavigation().extras.state.payment;

  }

  
  async presentPopover(ev: any, value: string) {
    const popover = await this.popoverController.create({
      component: ConsentpopoverPage,
      componentProps: { name: value },
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }




  forwardInternationalPayment() {
   var date: any = null;
   if(this.internationalForm.controls.internationalexecutiondate.value){
      date = this.internationalForm.controls.internationalexecutiondate.value.substr(0,10)
   }

    const payment = {
    requestedExecutionDate: date,
    instructedAmount: this.payment.instructedAmount,
    debtorAccount: this.payment.debtorAccount,
    creditorName: this.payment.creditorName,
    creditorAddress: this.payment.creditorAddress,
    creditorAccount: this.payment.creditorAccount,
    currencyOfTransfer: this.internationalForm.controls.currencyOfTransfer.value
    }
  
    this.router.navigate([ 'payments', 'payment-resume'], {state: { payment: payment, paymentType: 'INTERNATIONAL PAYMENT'}});
  }

  resetForm(){
    this.internationalForm.reset();
  }

  private initInternationalForm(): FormGroup {
    return this.formBuilder.group({
      internationalexecutiondate: null,
      internationalaccount: null,
      currencyOfTransfer: null,
      selectediban: null
    });
  }
}
