import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { PisService } from 'src/app/services/pis.service';
import { tap, mergeMap, filter, switchMap } from 'rxjs/operators';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-sepa',
  templateUrl: './sepa.component.html',
  styleUrls: ['./sepa.component.scss'],
})
export class SepaComponent implements OnInit {
  createForm: FormGroup;
  detailForm: FormGroup;
  cancelForm: FormGroup;
  paymentType: string;
  selectedSegment: 'CREATE' | 'DETAILS' | 'CANCEL';
  type: 'CREATE' | 'DETAILS' | 'CANCEL' = 'CREATE';
  constructor(private formBuilder: FormBuilder, private router: Router, private pisService: PisService, private sessionService: SessionService) { }

  ngOnInit() {
    this.paymentType = this.router.getCurrentNavigation().extras.state.paymentType;
    this.selectedSegment = this.router.getCurrentNavigation().extras.state.selectedSegment;
    console.log('selected segnet' + this.selectedSegment)
    if(this.selectedSegment != undefined){
      this.type = this.selectedSegment;
    }
    this.initCreateForm();
    this.initDetailsForm();
    this.initCancelForm();
  }


  private initCreateForm() {
    this.createForm = this.formBuilder.group({
      amount: [null, [ Validators.required ]]
    });
  }

  private initDetailsForm() {
    this.detailForm = this.formBuilder.group({
      paymentId: null,
      detailradio: null
      
    });
  }

  private initCancelForm() {
    this.cancelForm = this.formBuilder.group({
      cancelPaymentId: null
    });
  }

  segmentChanged(event: CustomEvent) {
    console.log('segment changed'+ event.detail.value)
    this.type = event.detail.value;

}



createPayment() {

  
    this.router.navigate(['payments', 'sepafrom'], {state: {amount: this.createForm.controls.amount.value, paymentType: this.paymentType}});
  
         
    }


    checkStatus(){

     
      const value = this.detailForm.controls.detailradio.value
      if(value == 'status'){
        this.pisService.getStatus(this.detailForm.controls.paymentId.value, this.paymentType).subscribe(p =>
        this.router.navigate(['payments', 'sepastatus'], {state: {paymentId: this.detailForm.controls.paymentId.value, paymentStatus: p.transactionStatus}})
        );
      }

      if(value == 'details'){
        this.pisService.getPayment(this.detailForm.controls.paymentId.value, this.paymentType).subscribe(p =>
        this.router.navigate(['payments', 'sepadetails'], {state: {payment: p}}));
        }
      }
    
    
  
      cancelPayment(){

        this.pisService.delete(this.cancelForm.controls.cancelPaymentId.value, this.paymentType)
      
        .pipe(
          filter(p => p != null && p != undefined && p._links != null),
          mergeMap(p => this.pisService.cancelAuth(p._links.startAuthorisation.href)
            .pipe(
              tap(s =>
                {const href = s._links.selectAuthenticationMethod.href;
                  this.sessionService.storePaymentId(this.cancelForm.controls.cancelPaymentId.value);
                  this.sessionService.storePaymentType('delete');
                  this.router.navigate(['/authorisation/paymentauthorisation'], {state: {scaMethods: s.scaMethods, href: href, paymentType: this.paymentType, mode: 'delete'}});}
              )),
           
              ))
              
              .pipe(tap(p => this.router.navigate(['/payments/submit'], {state: {paymentType: this.paymentType, mode: 'delete', paymentId: this.cancelForm.controls.cancelPaymentId.value}})))
              
              .subscribe();
        
                }
           
        
        // .subscribe(p => {
        //   console.warn('payment deletion started')
        //     if(p != undefined && p != null && p._links != null){
        //     const href = p._links.startAuthorisation.href;
           
      
        //     this.router.navigate(['authorisation'], {state: {href: href}});
        //     }
        //     else{
        //       this.cancelForm.reset();
        //         this.showCancelForm = false;
        //         this.showCancelDetails = true;
        //     }
        // })
       
      
      
      //   }
  
    // cancelPayment(){
    //   this.pisService.delete(this.detailForm.controls.paymentId.value).subscribe(p =>
    //   this.router.navigate(['tabs', 'tab', 'payments', 'sepacancel'], {state: {payment: p}}));
  
    // }

}
