import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SepacancelComponent } from './sepacancel.component';

describe('SepacancelComponent', () => {
  let component: SepacancelComponent;
  let fixture: ComponentFixture<SepacancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepacancelComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SepacancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
