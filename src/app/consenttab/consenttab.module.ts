import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsenttabPageRoutingModule } from './consenttab-routing.module';

import { ConsenttabPage } from './consenttab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsenttabPageRoutingModule
  ],
  declarations: [ConsenttabPage]
})
export class ConsenttabPageModule {}
