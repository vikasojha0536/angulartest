import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsenttabPage } from './consenttab.page';

const routes: Routes = [
  {
    path: '',
    component: ConsenttabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsenttabPageRoutingModule {}
