import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsenttabPage } from './consenttab.page';

describe('ConsenttabPage', () => {
  let component: ConsenttabPage;
  let fixture: ComponentFixture<ConsenttabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsenttabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsenttabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
