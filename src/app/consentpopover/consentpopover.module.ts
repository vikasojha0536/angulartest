import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsentpopoverPageRoutingModule } from './consentpopover-routing.module';

import { ConsentpopoverPage } from './consentpopover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsentpopoverPageRoutingModule
  ],
  declarations: [ConsentpopoverPage]
})
export class ConsentpopoverPageModule {}
