import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsentpopoverPage } from './consentpopover.page';

const routes: Routes = [
  {
    path: '',
    component: ConsentpopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsentpopoverPageRoutingModule {}
