import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-consentpopover',
  templateUrl: './consentpopover.page.html',
  styleUrls: ['./consentpopover.page.scss'],
})
export class ConsentpopoverPage implements OnInit {

  constructor(private navParams: NavParams) { }

  valueName: string;

  ngOnInit() {
    this.valueName  = this.navParams.get('name');
    console.log(this.navParams.get('name'))
  }

}
