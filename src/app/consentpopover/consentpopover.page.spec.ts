import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsentpopoverPage } from './consentpopover.page';

describe('ConsentpopoverPage', () => {
  let component: ConsentpopoverPage;
  let fixture: ComponentFixture<ConsentpopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentpopoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsentpopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
