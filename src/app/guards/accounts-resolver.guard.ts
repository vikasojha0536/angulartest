

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import {  Observable } from 'rxjs';




import { AisService } from '../services/ais.service';
import { BGAccounts } from '../models/account.model';

import { SessionService } from '../services/session.service';


@Injectable({
  providedIn: 'root'
})

export class AccountsResolverGuard implements Resolve<any> {
  //paymentType: string;
  constructor(private accountsService: AisService, private sessionService: SessionService) {}



  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BGAccounts> {
    console.warn('in the AccountResoverguard');

   const token: string = sessionStorage.getItem('Authtoken');
   const consentId: string = this.sessionService.getLastAddedConsentId();

    if(token != null && consentId != null){
      return this.accountsService.findBgAccounts()
    
    }
 
    
  }
}



