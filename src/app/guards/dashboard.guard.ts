





import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class DashboardGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const token: string = sessionStorage.getItem('Authtoken');
    const consentId: string = sessionStorage.getItem('consentId');
    if(token != null && consentId !=null)
{ return true;}

else{
  this.router.navigate(['tabs', 'tab', 'accounts', 'welcome']);
}
  }

 
}
