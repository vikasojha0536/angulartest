import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { AisService } from '../services/ais.service';
import { BGBalanceTransactions } from '../models/transaction.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionsResolverGuard implements Resolve<BGBalanceTransactions> {

  constructor(private router: Router, private aisService: AisService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BGBalanceTransactions> {
    const account = this.router.getCurrentNavigation().extras.state.account;
   // const aspsp = this.router.getCurrentNavigation().extras.state.aspsp;
    return this.aisService.findAllBgTransactions(account.resourceId, '2020-07-01', '');
  }
}
