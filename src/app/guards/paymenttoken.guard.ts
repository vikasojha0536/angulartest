import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PisService } from '../services/pis.service';
import { tap } from 'rxjs/operators';
import { Token } from '../models/authorisation.model';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class PaymenttokenGuard implements   Resolve<Token> {

  constructor(private paymentService: PisService, private sessionService: SessionService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Token>  {
    const code = route.queryParamMap.get('code');

    const body = new HttpParams()
      .set('client_id', environment.auth.clientId)
      .set('code', code)
      .set('code_verifier', 'M25iVXpKU3puUjFaYWg3T1NDTDQtcW1ROUY5YXlwalNoc0hhakxifmZHag')
      .set('grant_type', 'authorization_code')
      .set('redirect_uri', 'http://localhost:8100/payments/submit/')
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                
      return this.paymentService.token(body.toString(), headers)
      .pipe(tap(s => this.sessionService.storePaymentToken(s.access_token)));
  }
}
