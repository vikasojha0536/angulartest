import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AisService } from '../services/ais.service';
import { Token } from '../models/authorisation.model';
import { HttpParams, HttpHeaders } from '@angular/common/http';

import { tap, mergeMap, filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SessionService } from '../services/session.service';
import { LoadingService } from '../services/loading.service';




@Injectable({
  providedIn: 'root'
})
export class AuthorisationTokenResolverGuard implements Resolve<Token> {

  constructor(private accountService: AisService, private sessionService: SessionService,private router: Router, private loading: LoadingService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Token> {

    if(sessionStorage.getItem('Authtoken') != null && sessionStorage.getItem('Authtoken') != undefined){
      const token : Token= {
        access_token : sessionStorage.getItem('Authtoken')
      }
      return of(token);
    }
    var code = route.queryParamMap.get('code');

    const extraState = this.router.getCurrentNavigation().extras.state
    if(extraState != undefined){
      code = extraState.code;
    }
    

    const body = new HttpParams()
      .set('client_id', environment.auth.clientId)
      .set('code', code)
      .set('code_verifier', 'M25iVXpKU3puUjFaYWg3T1NDTDQtcW1ROUY5YXlwalNoc0hhakxifmZHag')
      .set('grant_type', 'authorization_code')
      .set('redirect_uri', 'http://localhost:8100/authorisation/token/')
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')




    return this.accountService.token(body.toString(), headers)
      .pipe(
       
        //  tap(s =>
        //   this.loading.hideLoader()

        //  ),
        mergeMap(t => this.accountService.refreshToken(t.refresh_token, headers)
          .pipe(
            tap(s =>
              this.sessionService.storeAccountToken(s.access_token)
            ))));

  }


}
