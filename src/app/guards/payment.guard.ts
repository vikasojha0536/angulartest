import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PisService } from '../services/pis.service';
import { BgPayment } from '../models/payment.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentGuard implements Resolve<BgPayment> {
  //paymentType: string;
  constructor(private paymentService: PisService, private router: Router) {}



  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BgPayment> {
    console.warn('in the AccountResoverguard');

   const token: string = sessionStorage.getItem('PaymentToken');
   const paymentId: string = sessionStorage.getItem('paymentId');

    if(token != null && paymentId != null){
      return this.paymentService.getPayment(paymentId, '');
    
    }
 
    
  }
}

