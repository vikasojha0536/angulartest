import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { ConsentsService } from '../services/consents.service';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class WelcomeGuard implements CanActivate {

  constructor(private router: Router, private sessionService: SessionService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    
    if(sessionStorage.getItem('Authtoken') != null && sessionStorage.getItem('Authtoken') != undefined 
    && this.sessionService.getLastAddedConsentId() != null){
      this.router.navigate(['/tabs/tab/accounts']);
    }
  else{
    return true;
  }
  }
}
