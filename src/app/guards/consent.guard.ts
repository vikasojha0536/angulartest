import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ConsentGuard implements CanActivate {
  
  constructor(private sessionService: SessionService, public alertController: AlertController, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<any> {
      if(this.sessionService.getLastAddedConsentId() != null &&  this.sessionService.getLastAddedConsentId() != undefined){
        return this.presentAlertConfirm();

     }
     else{
       return Promise.resolve(true);
     }
  }
  

  async presentAlertConfirm()   {
    return new Promise(async (resolve) => {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Adding Consent will <strong>invalidate</strong> the previous consent!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            resolve(false);
          }
        }, {
          text: 'Okay',
          handler: () => {
             sessionStorage.removeItem('consentId');
            sessionStorage.removeItem('Authtoken');
            resolve(true);
            // sessionStorage.removeItem('consentId');
            // sessionStorage.removeItem('Authtoken');
            // this.router.navigate(['/consents/specific']);
          
           
          }
        }
      ]
    });

    await alert.present();

     
  });
  
  }

}
