export const environment = {
  production: false,
  api: {
    window: false,
    host: 'https://api.psd2-2.bsummer.dxp.delivery/',
    env_port: '',
    key: '5366eeda-c7c1-4ff4-9d00-818757d13035',
    basepath: {
      ais: 'customer-position/customer/v1',
      pis: 'payment-order/customer/v1',
      auth: 'authentication/partner/v1',
      aspsps: 'participant-management/customer/v1',
      consents: 'customer-consent-management/customer/v1'
    }
  },
  auth: {
    client_id: 'mobileApp',
    secret: 'uKDtkZ'
  },
  test_data: {
    psu: '00N8TCRI',
    external_token: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMiwic3Vic2NyaXB0aW9uTnVtYmVyIjoiMDAwMDAwWFcifQ.kDYG5oZTF7crIt05sYQIgciF3axTEDxRiiunBdH7zoODlHCxBEp31Rq_3_aZlHj65J60CY7wlYjyLXOBTp1uRUOsyVOCVpp3u8jv8U9XlXvpfmZlKhf_nQp_maANze3XRUpvqJi1V12K1yRWR-qXTiwbPNSN0QIJei4yqYeB7Pk'
  }
};
