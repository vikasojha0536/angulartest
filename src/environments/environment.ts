

export const environment = {

  settings: {
    api: {
      host: 'http://localhost:9999/ui-backoffice',
      key: '',
      port: ''
    },
    openid: {
      authority: 'https://identity.int2.dxp.delivery/auth/',
      realm: 'realms/PSD2',
      client_id: 'psd2_ui-backoffice',
      response_type: 'code',
      root: 'http://localhost:4200/',
      scope: 'openid profile email',
    },
    language: {
      default: 'en',
      list: [
        {
          id: 'en',
          label: 'English',
          icons: ['GB']
        },
        {
          id: 'fr',
          label: 'Français',
          icons: ['FR']
        }
      ],
    },
    events: {},
    pagination: 10,
    aspsp_url_disable_regex_check: false,
    tpp_redirect_url_disable_regex_check: false,
    uris: {
      aspsps: '/aspsps/v1/aspsps',
      auth: '/server-external-auth/v1/token',
      consents_aspsp: '/cc/v1/consents',
      consents_tpp: '/cc-tpp/v1/consents',
      events: '/orderbook/v1/events',
      tpps: '/tpps/v1/tpps',
      psus: '/user/v1/users',
      regulatedEntities: '/regulated-entities/v1/regulated-entities',
    },
    acl: {
      actasaspsp_consents_view: ['admin', 'viewer'],
      actasaspsp_consents_delete: ['admin'],
      actasaspsp_psus_search: ['admin', 'viewer'],
      actasaspsp_psus_auditbook: ['admin', 'viewer'],
      actasaspsp_tpps_create: ['admin', 'tpp-manager'],
      actasaspsp_tpps_edit: ['admin', 'tpp-manager'],
      actasaspsp_tpps_details: ['admin', 'tpp-manager', 'viewer'],
      actasaspsp_tpps_auditbook: ['admin', 'tpp-manager', 'viewer'],
      actastpp_aspsps_create: ['admin'],
      actastpp_aspsps_edit: ['admin'],
      actastpp_aspsps_delete: ['admin'],
      actastpp_aspsps_details: ['admin', 'viewer'],
      actastpp_aspsps_auditbook: ['admin', 'tpp-manager', 'viewer'],
      actastpp_psus_search: ['admin', 'viewer'],
      actastpp_psus_auditbook: ['admin', 'viewer'],
      preta_regulatedEntities_details: ['admin', 'tpp-manager', 'viewer'],
      test: ['viewer']
    },
    modules: {
      actasaspsp: true,
      actastpp: true,
      preta: true,
      test: false,
    },
    security: {
      idle_max_duration_seconds: 6000,
      idle_countdown_seconds: 60,
      failure_tentatives: 5
    },
    notification: {
      timeout_interval_seconds: 0
    },
    external_registries: {
      actasaspsp: {
        operated_countries: ['BE', 'FR'],
        accreditations_mapping: {
          AIS: 'AIS',
          CIS: 'CBPII',
          PIS: 'PIS'
        }
      },
      actastpp: {
        host: 'http://localhost:9999/tink',
        uri: {
          providers: '/api/v1/providers'
        },
        markets: ['NO', 'DE', 'FI', 'BE', 'PT', 'BG', 'DK', 'FO', 'LV', 'FR', 'BR', 'SE', 'SG', 'SK', 'GB', 'IE', 'CA', 'MX', 'IT', 'GR', 'ES', 'AT', 'CZ', 'PL', 'RO', 'NL']
      }
    }
  },
  production: false,
  authenticationType: 'usernamepassword',
  private : true,
  api: {
    //host: 'https://api.int2.dxp.delivery/',
    //host: 'http://localhost:8080/',
    host: 'https://api.psd2-6.bsummer.dxp.delivery/',
    privateHost: 'https://api.int2.dxp.delivery/private',
    env_port: '30201',
   // key: 'bd5e79a1-3778-42a4-9230-4d1f641d531a', //psd2-5
    //key: '95f7c714-b282-49e7-93db-2ce2c924d90e', //psd2-1
    key: 'b15d4687-145e-41b2-95d4-68590fa3a7e9', //for psd2-6
   
    basepath: {
      ais: 'berlingroup/v1',
     // ais: 'api',
      bgauth: 'psd2/v1/berlingroup-auth',
      pis: 'berlingroup/v1'
    },
    resourceName: {
        accounts: '/accounts',
        token: '/token',
        transactions: '/transactions',
        consent: '/consents',
        payment: '/payments',
        sepaPayment: '/payments/sepa-credit-transfers',
        periodicPayment: '/periodic-payments/sepa-credit-transfers',
        wellknown: '.well-known'
    }
  },
  auth: {
    clientId: 'PSDBE-ALL-123456',
    secret: 'uKDtkZ'
  },
  test_data: {
    psu: '00000111',
    password: 'thaler',
    external_token: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMiwic3Vic2NyaXB0aW9uTnVtYmVyIjoiMDAwMDAwWFcifQ.kDYG5oZTF7crIt05sYQIgciF3axTEDxRiiunBdH7zoODlHCxBEp31Rq_3_aZlHj65J60CY7wlYjyLXOBTp1uRUOsyVOCVpp3u8jv8U9XlXvpfmZlKhf_nQp_maANze3XRUpvqJi1V12K1yRWR-qXTiwbPNSN0QIJei4yqYeB7Pk'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
