const fs = require('fs');
const PROXY_CONFIG = {
  "/api": {
    "target": {
      "host": "api.int2.dxp.delivery",
      "port": "443",
      "protocol": "https:",
      "pfx": fs.readFileSync('psdbe_ais_123456.p12')
    },
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  }
};
module.exports = PROXY_CONFIG;